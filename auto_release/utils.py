import numpy as np
from enum import IntEnum
from pc_ble_driver_py.observers import *

LIMB = 0
CHEST = 1
NAND_FLASH_CHEST_INDEX = 1
INIT_SUCCESS = 0
INIT_FAIL = 1
NRF_CONNECT_SUCCESS = 0

CONNECTION_SUCCESS = 0
CONNECTION_FAIL = 1
TIME_LIMIT = 60.0

REUSE_NAME = 0
PROGRAM_DEVICE_SENSOR = 0
UPDATE_DB_DEVICE_SENSOR = 1
REGISTER_DEVICE_SENSOR_DB = 2

class Chest_DataType(IntEnum):
    BQ25120 = 7
    NAND_FLASH = 6
    LSM6DSL = 5
    MAX30001 = 4
    MAX30205 = 3

class LIMB_DataType(IntEnum):
    BQ25120 = 7
    NAND_FLASH = 6
    MAX8614X = 5
    MAX30205 = 4
    
def choose_command_type():
    print('\t 0 : Program the device sensor')
    print('\t 1 : Update the Database Firmware')
    print('\t 2 : Register the device into Database')
    while True:
        try:
            choice = raw_input('Enter Program type: ')
            if ((choice == '0') or (choice == '1') or (choice == '2')):
                break
        except Exception:
            pass
        print ('\tTry again...')
    if choice == '0':
        return PROGRAM_DEVICE_SENSOR
    elif choice == '1':
        return UPDATE_DB_DEVICE_SENSOR
    else:
        return REGISTER_DEVICE_SENSOR_DB

def get_device_name():
    name = raw_input('Please Type the device name: ')
    return name

def choose_device_type():
    while True:
        try:
            choice = raw_input('Enter Device type(CHEST or LIMB): ')
            if ((choice == 'CHEST') or (choice == 'LIMB')):
                break
        except Exception:
            pass
        print ('\tTry again...')
    if choice == 'CHEST':
        return CHEST
    else:
        return LIMB

def convert_to_str(byte_arr): 

    # initialization of string to "" 
    new = "" 
    
    # traverse in the string  
    for x in byte_arr: 
        new += x  
    
    # return string  
    return new 

def convert_chest_bytes_to_dict(sys_status_chest):
    a = np.array(sys_status_chest, dtype=np.uint8)
    a = np.unpackbits(a,axis = 0)
    init_status = max(a[3:])
    dict = {}

    dict["device_status"] = "module_failed" if init_status else "success"

    dict["bq25120_init"] = "fail" if a[Chest_DataType.BQ25120] else "success"
    dict["max30205_init"] = "fail" if a[Chest_DataType.MAX30205] else "success"
    dict["max30001_init"] = "fail" if a[Chest_DataType.MAX30001] else "success"
    dict["nand_memory_init"] = "fail" if a[Chest_DataType.NAND_FLASH] else "success"
    dict["lsm6dsl_init"] = "fail" if a[Chest_DataType.LSM6DSL] else "success"

    return dict

def convert_limb_bytes_to_dict(sys_status_limb):
    dict = {}
    a = np.array(sys_status_limb, dtype=np.uint8)
    a = np.unpackbits(a,axis = 0)

    init_status = max(a[4:])

    dict["device_status"] = "module_failed" if init_status else "success"
    dict["bq25120_init"] = "fail" if a[LIMB_DataType.BQ25120] else "success"
    dict["max30205_init"] = "fail" if a[LIMB_DataType.MAX30205] else "success"
    dict["max8614x_init"] = "fail" if a[LIMB_DataType.MAX8614X] else "success"
    dict["nand_memory_init"] = "fail" if a[LIMB_DataType.NAND_FLASH] else "success"

    return dict
import os

def connection_test(serial_num):
    while True:
        reset_command = "nrfjprog -s " + str(serial_num) + " --reset"
        device_connection_status = os.system(reset_command)
        if(device_connection_status == NRF_CONNECT_SUCCESS):
            print("Device connected successfully")
            break
        else:
            print("Unable to connect to device. Retry it")
            while True:
                choice = raw_input('Type Y if you are ready to retry or Type N if you want to exit: ')
                if ((choice == 'Y') or (choice == 'N')):
                    if(choice == 'N'):
                        exit(1)
                    break
                else:
                    print("Please type the correct format")