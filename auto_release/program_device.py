import sys
import time
import Queue
import logging
import os
from os import path
import csv
import jinja2
from pc_ble_driver_py.observers     import *
from utils import *
import codecs

merge_command = "mergehex -m dfu/secure_bootloader/pca10040_ble/armgcc/s140_nrf52_6.0.0_softdevice.hex ble_nicu/ble_nicu_chest_v3/pca10056/s140/ses/Output/Release/Exe/settings.hex ble_nicu/ble_nicu_chest_v3/pca10056/s140/ses/Output/Release/Exe/chestv3.hex -o output.hex"

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "release_template.h"
t = templateEnv.get_template(TEMPLATE_FILE)

def program_dev(serial_num,dev_type,device_name):
    if dev_type == CHEST:
        build_Command = "emBuild -config \"Release\"  ../ble_nicu/ble_nicu_chest_v3/pca10056/s140/ses/ble_nicu_chest_v3_s140.emProject"
        app_command = "nrfjprog -s " +  str(serial_num) + " --program ../ble_nicu/ble_nicu_chest_v3/pca10056/s140/ses/Output/Release/Exe/chestv3.hex --sectorerase"
        settings_command = "nrfjprog -s " +  str(serial_num) + " --program ../ble_nicu/ble_nicu_chest_v3/pca10056/s140/ses/Output/Release/Exe/settings.hex --sectorerase"
        file_name = "chest_history_log.csv"
    else:
        build_Command = "emBuild -config \"Release\" ../ble_nicu/ble_nicu_limb_v3/pca10056/s140/ses/ble_nicu_limb_v3_s140.emProject"
        app_command = "nrfjprog -s " +  str(serial_num) + " --program ../ble_nicu/ble_nicu_limb_v3/pca10056/s140/ses/Output/Release/Exe/limbv3.hex --sectorerase"
        settings_command = "nrfjprog -s " +  str(serial_num) + " --program ../ble_nicu/ble_nicu_limb_v3/pca10056/s140/ses/Output/Release/Exe/settings.hex --sectorerase"
        file_name = "limb_history_log.csv"

    fieldnames = ['Serial Number','Device name']

    if os.path.exists(file_name) == False: 
        csvfile = open(file_name,"w+")
        writer = csv.writer(csvfile, delimiter=' ',quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(fieldnames)

    rt = t.render(device_name = "\"" + device_name + "\"")

    ofh = codecs.open("release_config.h","w", encoding="utf-8")
    ofh.write(rt)
    ofh.close()

    err_code = os.system(build_Command)
    if(err_code != INIT_SUCCESS):
        return INIT_FAIL

    dfu_command = "nrfjprog -s " + str(serial_num) + " --program ../dfu/secure_bootloader/pca10056_ble/armgcc/_build/nrf52840_xxaa_s140.hex --sectorerase"
    err_code = os.system(dfu_command)
    if(err_code != INIT_SUCCESS):
        return INIT_FAIL

    soft_device_command = "nrfjprog -s " +  str(serial_num) + " --program ../sdk/nRF5_SDK/components/softdevice/s140/hex/s140_nrf52_6.0.0_softdevice.hex --sectorerase"
    err_code = os.system(soft_device_command)
    if(err_code != INIT_SUCCESS):
        return INIT_FAIL

    err_code = os.system(app_command)
    if(err_code != INIT_SUCCESS):
        return INIT_FAIL
    err_code = os.system(settings_command)
    if(err_code != INIT_SUCCESS):
        return INIT_FAIL
    reset_command = "nrfjprog -s " +  str(serial_num) + " --reset"
    err_code = os.system(reset_command)
    if(err_code != INIT_SUCCESS):
        return INIT_FAIL

    csvfile = open(file_name,"a")
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow([serial_num,device_name])
    print("Programming to device has done. Device name is : ",device_name)
    return INIT_SUCCESS

