import mysql.connector
from utils import *
from mysql.connector import errorcode

def connect_db():
  db = mysql.connector.connect(
    host="10.1.10.67",
    user="simon.kim",
    passwd="Sibel2019",
    database="janssen_device_sensor"
  )
  return db

def set_up_tables():
  TABLES = {}
  TABLES['chest'] = (
      "CREATE TABLE `chest` ("
      "  `creation_id` int(11) NOT NULL AUTO_INCREMENT,"
      "  `device_name` VARCHAR(4) NOT NULL,"
      "  `creation_date` DATETIME DEFAULT CURRENT_TIMESTAMP,"
      "  `bq25120_init` ENUM('success','fail'),"
      "  `max30205_init` ENUM('success','fail'),"
      "  `lsm6dsl_init` ENUM('success','fail'),"
      "  `max30001_init` ENUM('success','fail'),"
      "  `nand_memory_init` ENUM('success','fail'),"
      "  `bluetooth_connection` ENUM ('success','fail'),"
      "  `device_status` ENUM ('success','fail','initializing','failed_but_replaced','module_failed') NOT NULL,"
      "  `firmware_ver` VARCHAR(10),"
      "  `hardware_ver` VARCHAR(10),"
      "  `device_programmer` VARCHAR(20) NOT NULL,"
      "  PRIMARY KEY (`creation_id`)"
      ") ENGINE=InnoDB")

  TABLES['limb'] = (
      "CREATE TABLE `limb` ("
      "  `creation_id` int(11) NOT NULL AUTO_INCREMENT,"
      "  `device_name` VARCHAR(4) NOT NULL,"
      "  `creation_date` DATETIME DEFAULT CURRENT_TIMESTAMP,"
      "  `bq25120_init` ENUM ('success','fail'),"
      "  `max30205_init` ENUM ('success','fail'),"
      "  `nand_memory_init` ENUM ('success','fail'),"
      "  `max8614x_init` ENUM ('success','fail'),"
      "  `bluetooth_connection` ENUM ('success','fail'),"
      "  `device_status` ENUM ('success','fail','initializing','failed_but_replaced','module_failed') NOT NULL,"
      "  `firmware_ver` VARCHAR(10),"
      "  `hardware_ver` VARCHAR(10),"
      "  `device_programmer` VARCHAR(20) NOT NULL,"
      "  PRIMARY KEY (`creation_id`)"
      ") ENGINE=InnoDB")
  TABLES['programmer'] = (
      "CREATE TABLE `programmer` ("
      "  `programmer_id` int(11) NOT NULL AUTO_INCREMENT,"
      "  `programmer_name` VARCHAR(20) NOT NULL,"
      "  PRIMARY KEY (`programmer_id`)"
      ") ENGINE=InnoDB")
  db = connect_db()
  cursor = db.cursor()


  for table_name in TABLES:
    table_description = TABLES[table_name]
    try:
        print("Creating table {}: ".format(table_name))
        cursor.execute(table_description)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

  cursor.close()
  db.close()

def get_new_dev_id(dev_type,programmer_name):
  db = connect_db()
  cursor = db.cursor(dictionary=True,buffered=True)

  if(dev_type == CHEST):
    lock_table = ("LOCK TABLE chest WRITE")
    get_failed_id = ("SELECT creation_id, device_name from chest WHERE device_status = 'fail' ORDER BY device_name ASC")
    update_failed_device = ("UPDATE chest SET device_status = %s WHERE creation_id = %s")
    get_latest_id = ("SELECT creation_id,device_name from chest ORDER BY creation_id DESC")
    add_device = ("INSERT INTO chest (device_name, device_status,device_programmer) VALUES (%s, %s, %s)")
    get_created_id = ("SELECT creation_id from chest ORDER BY creation_id DESC")
    device_name_pre = "C"
  else:
    lock_table = ("LOCK TABLE limb WRITE")
    get_failed_id = ("SELECT creation_id, device_name from limb WHERE device_status = 'fail' ORDER BY device_name ASC")
    update_failed_device = ("UPDATE limb SET device_status = %s WHERE creation_id = %s")
    get_latest_id = ("SELECT creation_id, device_name from limb ORDER BY creation_id DESC")
    add_device = ("INSERT INTO limb (device_name, device_status,device_programmer) VALUES (%s, %s, %s)")
    get_created_id = ("SELECT creation_id from limb ORDER BY creation_id DESC")
    device_name_pre = "L"

  cursor.execute(lock_table)

  cursor.execute(get_failed_id)

  count = cursor.rowcount
  if count != 0:
    head_row = cursor.fetchmany(size=1)
    cursor.fetchall()
    creation_id = head_row[0]['creation_id']
    device_name = head_row[0]['device_name']
    cursor.execute(update_failed_device,('failed_but_replaced',creation_id))
  else:
    cursor.execute(get_latest_id)
    count = cursor.rowcount
    if count > 0:
      head_row = cursor.fetchmany(size=1)
      print(head_row[0])
      device_id = int(head_row[0]['device_name'][len(device_name_pre):]) + 1
      print("DEVICE ID: {}".format(device_id))
      if device_id > 999:
        device_name = device_name_pre + "{0:0=4d}".format(device_id)
      else:
        device_name = device_name_pre + "{0:0=3d}".format(device_id)
    else:
      device_name = device_name_pre + "001"
  cursor.execute(add_device,(device_name,"initializing",programmer_name))

  cursor.execute(get_created_id)
  head_row = cursor.fetchmany(size=10)
  creation_id = head_row[0]['creation_id']

  unlock_tables = ("UNLOCK TABLES")
  cursor.execute(unlock_tables)

  cursor.close()
  db.close()
  return device_name,creation_id

def update_db(dev_type,creation_id,system_status,firmware_ver,hardware_ver,address_string):
  if(dev_type == CHEST):
    sys_status_dict = convert_chest_bytes_to_dict(system_status)
    update_device = ("UPDATE chest SET "
                     "firmware_ver = %(firmware_ver)s, "
                     "hardware_ver = %(hardware_ver)s, "
                     "bq25120_init = %(bq25120_init)s, "
                     "lsm6dsl_init = %(lsm6dsl_init)s, "
                     "max30001_init = %(max30001_init)s, "
                     "max30205_init = %(max30205_init)s, "
                     "nand_memory_init = %(nand_memory_init)s, "
                     "bluetooth_connection = 'success', "
                     "bluetooth_address = %(bluetooth_address)s, "
                     "device_status = %(device_status)s "
                     "WHERE creation_id = %(creation_id)s")
    lock_table = ("LOCK TABLES chest WRITE")
  else:
    sys_status_dict = convert_limb_bytes_to_dict(system_status)
    update_device = ("UPDATE limb SET "
                     "firmware_ver = %(firmware_ver)s, "
                     "hardware_ver = %(hardware_ver)s, "
                     "bq25120_init = %(bq25120_init)s, "
                     "max30205_init = %(max30205_init)s, "
                     "max8614x_init = %(max8614x_init)s, "
                     "nand_memory_init = %(nand_memory_init)s, "
                     "bluetooth_connection = 'success', "
                     "bluetooth_address = %(bluetooth_address)s, "
                     "device_status = %(device_status)s "
                     "WHERE creation_id = %(creation_id)s")
    lock_table = ("LOCK TABLES limb WRITE")

  sys_status_dict["creation_id"] = creation_id
  sys_status_dict["firmware_ver"] = str(firmware_ver)
  sys_status_dict["hardware_ver"] = str(hardware_ver)
  sys_status_dict["bluetooth_address"] = address_string

  db = connect_db()
  cursor = db.cursor()

  cursor.execute(lock_table)

  r = cursor.execute(update_device,sys_status_dict)
  print("hihi",sys_status_dict,r)
  
  unlock_tables = ("UNLOCK TABLES")
  cursor.execute(unlock_tables)

  cursor.close()
  db.close()
  return sys_status_dict

def report_fail(creation_id,dev_type):
  db = connect_db()
  cursor = db.cursor()
 
  if(dev_type == CHEST):
    lock_table = ("LOCK TABLE chest WRITE")
    report_failed_device = ("UPDATE chest SET device_status = 'fail' WHERE creation_id = %s")
  else:
    lock_table = ("LOCK TABLE limb WRITE")
    report_failed_device = ("UPDATE limb SET device_status = 'fail' WHERE creation_id = %s")

  cursor.execute(lock_table)
  cursor.execute(report_failed_device,(creation_id,))
  unlock_tables = ("UNLOCK TABLES")
  cursor.execute(unlock_tables)
  cursor.close()
  db.close()

def report_bluetooth_fail(creation_id,dev_type):
  db = connect_db()
  cursor = db.cursor()
 
  if(dev_type == CHEST):
    lock_table = ("LOCK TABLE chest WRITE")
    report_failed_device = ("UPDATE chest SET bluetooth_connection = 'fail', device_status = 'module_failed' WHERE creation_id = %s")
  else:
    lock_table = ("LOCK TABLE limb WRITE")
    report_failed_device = ("UPDATE limb SET bluetooth_connection = 'fail' device_status = 'module_failed' WHERE creation_id = %s")

  cursor.execute(lock_table)
  cursor.execute(report_failed_device,(creation_id,))
  unlock_tables = ("UNLOCK TABLES")
  cursor.execute(unlock_tables)
  cursor.close()
  db.close()

def register_name():
  while True:
      try:
          name = raw_input('Enter Your Name for Registeration: ')
          print('Your name is : {}'.format(name))
          check = raw_input('Is it correct? (Y or N) :')
          if check == 'Y' or check == 'N':
              if(check == 'Y'):
                if put_name_into_db(name) == True:
                  return name
                else:
                  check = raw_input('Would like to use registered name? (Y or N) :')
                  if(check == 'Y'):
                    return REUSE_NAME
                  else:
                    continue
              else:
                  ("Type your name again")
          else:
              print("Please type the correct format")
      except Exception:
          exit(1)


def put_name_into_db(name):

  add_name = ("INSERT INTO programmer (programmer_name) VALUE (%s)")
  lock_table = ("LOCK TABLES programmer WRITE")
  get_existing_id = ("SELECT * from programmer WHERE programmer_name = %s")

  db = connect_db()
  cursor = db.cursor(dictionary=True,buffered=True)

  cursor.execute(lock_table)
  cursor.execute(get_existing_id,(name,))

  count = cursor.rowcount
  cursor.fetchall()
  if count > 0:
    print("Name already exist in the database. Put different name")
    cursor.execute("UNLOCK TABLES")
    cursor.close()
    db.close()
    return False
  
  cursor.execute(add_name,(name,))

  cursor.execute("UNLOCK TABLES")
  cursor.close()
  db.close()
  return True


def check_name(name):

  lock_table = ("LOCK TABLES programmer WRITE")
  get_existing_id = ("SELECT * from programmer WHERE programmer_name = %s")

  db = connect_db()
  cursor = db.cursor(dictionary=True,buffered=True)

  cursor.execute(lock_table)
  cursor.execute(get_existing_id,(name,))

  count = cursor.rowcount
  cursor.fetchall()
  if count > 0:
    cursor.execute("UNLOCK TABLES")
    cursor.close()
    db.close()
    return True
  
  cursor.execute("UNLOCK TABLES")
  cursor.close()
  db.close()
  return False

def get_programmer_name():
    while True:
        try:
            name = raw_input('Enter Your Name:')
            if check_name(name) == True:
                print("Find your name in the registeration board")
                return name
            else:
                print("Can't find your name in the registeration board.")
                while True:
                    check = raw_input("Would you like to register your name? (Y for register N for retry) :")
                    if check =='Y' or check == 'N':
                        if check == 'Y':
                            name = register_name()
                            if(name == REUSE_NAME):
                                break
                            else:
                                return name
                        else:
                            break
                    else:
                        print("Please type the correct format")
        except:
            pass


def update_device_firmware(dev_name,dev_type,firmware_ver,address_string):
  sys_status_dict = {}
  if(dev_type == CHEST):
    update_device = ("UPDATE chest SET "
                     "firmware_ver = %(firmware_ver)s, "
                     "bluetooth_connection = 'success', "
                     "bluetooth_address = %(bluetooth_address)s "
                     "WHERE creation_id = %(creation_id)s")
    lock_table = ("LOCK TABLES chest WRITE")
    get_existing_id = ("SELECT * FROM chest WHERE device_name = %s AND device_status = 'success'")

  else:
    update_device = ("UPDATE limb SET "
                     "firmware_ver = %(firmware_ver)s, "
                     "bluetooth_connection = 'success', "
                     "bluetooth_address = %(bluetooth_address)s "
                     "WHERE creation_id = %(creation_id)s")
    lock_table = ("LOCK TABLES limb WRITE")
    get_existing_id = ("SELECT * FROM limb WHERE device_name = %s AND device_status = 'success'")

  db = connect_db()
  cursor = db.cursor(dictionary=True,buffered=True)

  cursor.execute(lock_table)
  cursor.execute(get_existing_id,(dev_name,))
  count = cursor.rowcount
  head_row = cursor.fetchall()
  if count < 1:
    print("Cannot find the device name in the database")
    unlock_tables = ("UNLOCK TABLES")
    cursor.execute(unlock_tables)

    cursor.close()
    db.close()
    return
  creation_id = head_row[0]['creation_id']
  print(address_string)
  sys_status_dict["creation_id"] = creation_id
  sys_status_dict["firmware_ver"] = str(firmware_ver)
  sys_status_dict["bluetooth_address"] = address_string

  cursor.execute(update_device,sys_status_dict)
  unlock_tables = ("UNLOCK TABLES")
  cursor.execute(unlock_tables)

  cursor.close()
  db.close()

def insert_device_into_db(programmer_name,dev_name,dev_type,system_status,firmware_ver,hardware_ver,address_string):
  db = connect_db()
  cursor = db.cursor(dictionary=True,buffered=True)

  if(dev_type == CHEST):
    sys_status_dict = convert_chest_bytes_to_dict(system_status)
    update_device = ("INSERT chest SET "
                     "device_name = %(device_name)s, "
                     "firmware_ver = %(firmware_ver)s, "
                     "hardware_ver = %(hardware_ver)s, "
                     "bq25120_init = %(bq25120_init)s, "
                     "lsm6dsl_init = %(lsm6dsl_init)s, "
                     "max30001_init = %(max30001_init)s, "
                     "max30205_init = %(max30205_init)s, "
                     "nand_memory_init = %(nand_memory_init)s, "
                     "device_programmer = %(device_programmer)s, "
                     "bluetooth_connection = 'success', "
                     "bluetooth_address = %(bluetooth_address)s, "
                     "device_status = %(device_status)s ")
    lock_table = ("LOCK TABLES chest WRITE")
    get_existing_id = ("SELECT * FROM chest WHERE device_name = %s AND device_status = 'success'")
  else:
    sys_status_dict = convert_limb_bytes_to_dict(system_status)
    update_device = ("INSERT limb SET "
                     "device_name = %(device_name)s, "
                     "firmware_ver = %(firmware_ver)s, "
                     "hardware_ver = %(hardware_ver)s, "
                     "bq25120_init = %(bq25120_init)s, "
                     "max30205_init = %(max30205_init)s, "
                     "max8614x_init = %(max8614x_init)s, "
                     "nand_memory_init = %(nand_memory_init)s, "
                     "device_programmer = %(device_programmer)s, "
                     "bluetooth_connection = 'success', "
                     "bluetooth_address = %(bluetooth_address)s, "
                     "device_status = %(device_status)s ")
    lock_table = ("LOCK TABLES limb WRITE")
    get_existing_id = ("SELECT * FROM limb WHERE device_name = %s AND device_status = 'success'")

  sys_status_dict["firmware_ver"] = str(firmware_ver)
  sys_status_dict["hardware_ver"] = str(hardware_ver)
  sys_status_dict["bluetooth_address"] = address_string
  sys_status_dict["device_name"] = dev_name
  sys_status_dict["device_programmer"] = programmer_name

  cursor.execute(lock_table)

  cursor.execute(lock_table)
  cursor.execute(get_existing_id,(dev_name,))
  count = cursor.rowcount
  head_row = cursor.fetchall()
  if count >= 1:
    print("Device already exist in the database")
    print("Fail to register")
    unlock_tables = ("UNLOCK TABLES")
    cursor.execute(unlock_tables)

    cursor.close()
    db.close()
    return

  cursor.execute(update_device,sys_status_dict)
  unlock_tables = ("UNLOCK TABLES")
  cursor.execute(unlock_tables)

  cursor.close()
  db.close()
  
  print("DEVICE STATUS FOR {}: {}".format(dev_name,sys_status_dict))
  return sys_status_dict