from program_device import program_dev
from utils import *
from database_command import *
from pc_ble_driver_py.observers import *
from recieve_command import *
import pyfiglet
from pyfiglet import Figlet

from termcolor import colored

ascii_banner = pyfiglet.figlet_format("-SIBEL HEALTH-")
print(ascii_banner)
application = Figlet(font='small')
print("#"*75)
print(application.renderText('Sensor Programmer'))
print("ver 0.0")
print("#"*75)


def program_device_main():
        programmer_name = get_programmer_name()
        while True:
                # choice_program_dev_ser_num = device_choose()
                dev_type = choose_device_type()
                connection_test("682334108")
                dev_name,creation_id = get_new_dev_id(dev_type,programmer_name)
                if(program_dev("682334108",dev_type,dev_name) == INIT_FAIL):
                        report_fail(creation_id,dev_type)
                        print("PRGROMMING TO DEVICE HAS FAILED. Device name: {} will be resued".format(dev_name))
                        continue
                else:
                        system_status, firmware_ver, hardware_ver, address_string, connection_status  = read_system_signal(dev_name,creation_id,dev_type)
                        if (connection_status == CONNECTION_FAIL):
                                report_bluetooth_fail(creation_id,dev_type)
                                print("Timeout for Connection has happened")
                                continue
                status_dict = update_db(dev_type,creation_id,system_status,firmware_ver,hardware_ver,address_string)
                if(status_dict["device_status"] == 'success'):
                        print("DEVICE HAS BEEN INITIALIZED {}".format(colored("SUCCESSFULLY", 'green')))
                else:
                        print("DEVICE HAS BEEN INITIALIZED WITH SOME {}".format(colored("ERROR", 'red')))
                print("DEVICE STATUS FOR {}: ".format(dev_name))
                for key, value in status_dict.items():
                        print("{}{}: {}".format(key,(20 - len(key)) * " ",value))

def update_db_fimware_main():
        print("This program will automatically update Bluetooth Address inside of Database")
        while True:
                dev_name = get_device_name()
                dev_type = choose_device_type()
                system_status, firmware_ver, hardware_ver, address_string,connection_status  = read_system_signal(dev_name)
                if(connection_status == CONNECTION_FAIL):
                        print("Timeout for Connection has happened. Please Type the correct device name")
                        continue
                update_device_firmware(dev_name,dev_type,firmware_ver,address_string)


def register_new_deivce_main():
        print("This program will register new device into Database")
        programmer_name = get_programmer_name()
        while True:
                dev_name = get_device_name()
                dev_type = choose_device_type()
                system_status, firmware_ver, hardware_ver, address_string,connection_status  = read_system_signal(dev_name)
                if(connection_status == CONNECTION_FAIL):
                        print("Timeout for Connection has happened. Please Type the correct device name")
                        continue
                print()
                insert_device_into_db(programmer_name,dev_name,dev_type,system_status,firmware_ver,hardware_ver,address_string)


if __name__ == "__main__":
    init("NRF52")
    choice = choose_command_type()
    if(choice == PROGRAM_DEVICE_SENSOR):
            program_device_main()
    elif(choice == UPDATE_DB_DEVICE_SENSOR):
            update_db_fimware_main()
    else:
            register_new_deivce_main()
