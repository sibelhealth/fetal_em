#ifndef PIN_CONFIG_H
#define PIN_CONFIG_H

#define VERSION_MAJOR 1
#define VERSION_MINOR 0

#define SESSION_TIMEOUT     APP_TIMER_TICKS(1000* 60 * 60 * 8)

/**< TX Power Level value. This will be set both in the TX Power service, in the advertising data, and also used to set the radio transmit power. */
#define TX_POWER_LEVEL                      (4)  

/** BQ25120A */
#define BQ_SCL_PIN           14
#define BQ_SDA_PIN           12
#define BQ_CD_PIN             8

/** Memory */
#define MT29F_SS_PIN          6
#define MT29F_SCK_PIN         5
#define MT29F_MISO_PIN        4
#define MT29F_MOSI_PIN       11
#define MT29F_HOLD_PIN        4
#define MT29F_WP_PIN          3

/** ADS1299 */
#define ADS1299_SS_PIN       20
#define ADS1299_MOSI_PIN     15
#define ADS1299_MISO_PIN     17
#define ADS1299_DRDY_PIN     24
#define ADS1299_SCK_PIN      18
#define ADS1299_PWDN_PIN     23
#define ADS1299_RESET_PIN    16

/** TPS */
#define TPS_3V_CTRL_PIN 27
#define TPS_5V_CTRL_PIN 29

#define INDICATION_LED_PIN   28

/*Choose one of the Radio mode*/
#define TS_RX_MODE 0
#define TS_TX_MODE 1

/*Choose one of timer mode*/
#define TS_TIMER_MODE 0
#define TS_RTC_MODE 1

#define TS_DEBUG_PIN_NUM        2
#define TS_DEBUG_GPIOTE_NUM     3
#define TS_DEBUG_GPIOTE_TASK    NRF_GPIOTE_TASKS_OUT_3
#define TS_DEBUG_PPI_CHAN       NRF_PPI_CHANNEL0
#define TS_DEBUG_RX_PIN_NUM     28

#if (TS_TIMER_MODE)   //TIMER MODE
    #define SYNC_TIMER NRF_TIMER3
#elif (TS_RTC_MODE)
   #define SYNC_TIMER NRF_RTC2
#endif
#endif // PIN_CONFIG_H