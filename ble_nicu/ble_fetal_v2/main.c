/**
 * Copyright (c) 2014 - 2018, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** 
 *
 * @brief Fetal Monitoring Application main file.
 *
 */

#include "nrf_dfu_ble_svci_bond_sharing.h"
#include "nrf_svci_async_function.h"
#include "nrf_svci_async_handler.h"

#include "app_config.h"
#include "sh_common.h"

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_sdm.h"
#include "app_error.h"
#include "app_scheduler.h"
#include "ble.h"
#include "ble_err.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_dis.h"
#include "ble_dfu.h"
#include "ble_sys.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "app_timer.h"
#include "nrf_drv_rtc.h"
#include "peer_manager.h"
#include "fds.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "ble_conn_state.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_power.h"
#include "nrf_delay.h"
#include "nrf_drv_saadc.h"
#include "nrf_bootloader_info.h"
#include "nrf_ppi.h"
#include "nrfx_timer.h"
#include "nrf_twi_mngr.h"
#include "nrfx_wdt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrfx_gpiote.h"
#include "nrfx_spim.h"
#include "nrfx_twi.h"
#include "nrfx_wdt.h"

#include "ads1299.h"
#include "ads1299_com.h"
#include "m_data_logging.h"
#include "m_time_sync.h"
#include "bq25120.h"
#include "lsm6dsl.h"
#include "ble_sys.h"
#include "ble_bas.h"
#include "ble_biop.h"

#include "arm_math.h"
#include "firCoeffsf32.h"

/*lint -save -e689 */ /* Apparent end of comment ignored */
#include "arm_const_structs.h"
/*lint -restore */

#define DEVICE_NAME                         "F100"                            /**< Name of device. Will be included in the advertising data. */

/**App Identifer */
#define APP_COMPANY_IDENTIFIER              0xFFFF
#define MANUFACTURER_NAME                   "SibelHealth"                   /**< Manufacturer. Will be passed to Device Information Service. */

#define APP_ADV_INTERVAL                    600                                     /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_DURATION                    300                                   /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

#define APP_BLE_CONN_CFG_TAG                1                                       /**< A tag identifying the SoftDevice BLE configuration. */
#define APP_BLE_OBSERVER_PRIO               3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define MIN_CONN_INTERVAL                   MSEC_TO_UNITS(300, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.4 seconds). */
#define MAX_CONN_INTERVAL                   MSEC_TO_UNITS(500, UNIT_1_25_MS)       /**< Maximum acceptable connection interval (0.65 second). */
#define SLAVE_LATENCY                       1                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                    MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory timeout (4 seconds). */

#define BIOP_BUF_SIZE                       96
#define APP_MTU_SIZE                        185 
#define APP_DLE_SIZE                        189

#define FIRST_CONN_PARAMS_UPDATE_DELAY      APP_TIMER_TICKS(5000)                   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY       APP_TIMER_TICKS(30000)                  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT        3                                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                      1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                      0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                      0                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS                  0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                       0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE              7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE              16                                      /**< Maximum encryption key size. */

#define DEAD_BEEF                           0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

/** Stream Service */
#define MIN_STREAM_CONN_INTERVAL_MS MSEC_TO_UNITS(15, UNIT_1_25_MS)
#define MAX_STREAM_CONN_INTERVAL_MS MSEC_TO_UNITS(15, UNIT_1_25_MS)

/** BLE Related instance DEF */
BLE_BIOP_DEF(m_ble_biop);
BLE_BAS_DEF(m_bas);
BLE_SYS_DEF(m_ble_sys);

/** App Timer Def */
APP_TIMER_DEF(m_pmic_timer_id); 
APP_TIMER_DEF(m_module_wdt_timer_id);
APP_TIMER_DEF(m_session_timer_id);
APP_TIMER_DEF(m_crit_bat_timer_id);

/** SPI */
#define SPI_INSTANCE 1                                              /**< SPI instance index. */
static const nrfx_spim_t dl_spi = NRFX_SPIM_INSTANCE(SPI_INSTANCE); /**< SPI instance. */

/** SPI Instances*/
//#define SPI_INSTANCE_1 1                                           /**< SPI instance index. */
#define SPI_FAILED_CNT_LIMIT 20
//static const nrfx_spim_t sensor_spi = NRFX_SPIM_INSTANCE(SPI_INSTANCE_1); /**< SPI instance. */
//static volatile bool spi_xfer_done;                                /**< Flag used to indicate that SPI instance completed the transfer. */

/** TWI Instance */
#define TWI_INSTANCE_ID     0
#define MAX_PENDING_TRANSACTIONS    5

static uint8_t        twi_buf[32];   
NRF_TWI_MNGR_DEF(m_nrf_twi_mngr, MAX_PENDING_TRANSACTIONS, TWI_INSTANCE_ID);

/** Data Logging */
static sh_dl_nand_page_t biop_nand_pages[2];
static uint8_t           biop_page_index = 0;
static uint16_t          biop_buf_index  = 0;
static sh_dl_nand_page_t imu_nand_pages[2];
static uint8_t           imu_page_index = 0;
static uint16_t          imu_buf_index  = 0;
static volatile bool     m_active_session = false;

/** PMIC */
#define PMIC_READ_INTERVAL_MS 1000
#define PMIC_NON_CHARGE_READ_SEC (5)
#define PMIC_READ_INTERVAL APP_TIMER_TICKS(PMIC_READ_INTERVAL_MS) /**< Battery level measurement interval (ticks). */
#define PMIC_IS_BAT_CRIT_READ_INTERVAL_MS 10000
#define PMIC_IS_BAT_CRIT_READ_INTERVAL APP_TIMER_TICKS(PMIC_IS_BAT_CRIT_READ_INTERVAL_MS) 
static bool interrupt_flag = false;
struct bq25120_dev  pmic_dev;
pmic_battery_info_t battery_info;
static uint16_t pmic_read_cnt = 0;


/** IMU */
#define ACCEL_FIFO_TH_SAMPLES (3 * 24)
#define ACCEL_FIFO_RESET_TH (ACCEL_FIFO_TH_SAMPLES * 2)
#define ACCEL_STREAM_ITERATION 6 // 17.3 Hz
#define ACCEL_STREAM_SIZE_BYTES        24
uint8_t accel_stream_buf[ACCEL_STREAM_SIZE_BYTES];
static uint16_t accel_stream_buf_index = 0;

static lsm6dsl_dev_t     m_imu_dev;

/* ADS1299 */
static ads1299_dev    m_ads1299_dev[ADS1299_MAX_NUM_DAISY_CHNS];
static float          cos_accum[ADS1299_MAX_NUM_CHNLS];
static float          sin_accum[ADS1299_MAX_NUM_CHNLS];
static const float    cos_table[] = {1, 0, -1, 0};
static const float    sin_table[] = {0, 1, 0, -1};
static uint32_t       m_ads1299_sample = 0;
static float          m_chnl_imped[ADS1299_MAX_NUM_CHNLS];

//TODO: remove debug code
//#define LOFF_TEST_6NA
//#define SAVE_IMPEDANCE_VALS
//#define SEND_IMPED_VOLTAGES

/** System Status */
static sys_status_t sys_status;

/** Scheduler Define */
#define SCHED_MAX_EVENT_DATA_SIZE (sizeof(ble_gap_conn_params_t)) /**< Maximum size of scheduler events. */
#define SCHED_QUEUE_SIZE 10                              /**< Maximum number of events in the scheduler queue. */

/** Time Sync */
static ts_params_t ts_params;
static uint8_t     rf_address[5] = {0xDE, 0xAA, 0xBE, 0xEF, 0x94};

/** Module */
static volatile bool m_modules_initalized = false;

/** Module WDT */
#define MODULE_WDT_INTERVAL APP_TIMER_TICKS(20000) 
#define WDT_CNT_LIMIT 1
static uint8_t ads1299_wdt;
static uint8_t lsm6dsl_wdt;

/** System WDT */
#define WDT_RELOAD_VAL_MS           (1000 * 60 * 2) // 2 Minutes
nrfx_wdt_channel_id m_channel_id;

/** SPI */
static uint8_t m_tx_buf[1024]; /**< RX buffer. */
static uint8_t m_rx_buf[1024]; /**< RX buffer. */

/** BLE */
static uint8_t biop_stream_buf[2][BIOP_BUF_SIZE];
static uint8_t biop_stream_index = 0;
static uint8_t biop_stream_buf_idx = 0;
static uint8_t biop_down_sample_cnt = 0;
static uint8_t biop_imped_down_sample_cnt = 0;
static bool    m_ble_stream_biop;
static volatile bool app_sched_p5_flag = false;
static sh_ble_adv_data_t m_adv_data;

/** Sensor Type Info */
static uint8_t device_type = SH_DEV_TYPE_FETAL;

NRF_BLE_GATT_DEF(m_gatt);                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                 /**< Advertising module instance. */
APP_TIMER_DEF(m_saadc_timer_id);                     /**< Heart rate measurement timer. */

static uint16_t m_conn_handle         = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */

static nrfx_spim_t* p_spi;
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

#define USE_FFT
#ifdef USE_FFT
/** FFT Info */
#define NFFT_SAMPLES        1024   //!< Complex numbers input data array size. Correspond to FFT calculation this number must be power of two starting from 2^5 (2^4 pairs) with maximum value 2^13 (2^12 pairs).

static uint16_t  m_fft_count;
static uint32_t  m_ifft_flag             = 0;         //!< Flag that selects forward (0) or inverse (1) transform.
static uint32_t  m_do_bit_reverse        = 1;         //!< Flag that enables (1) or disables (0) bit reversal of output.
static float32_t m_fft_input_f32[NFFT_SAMPLES*2];     //!< FFT input array. Time domain.
static float32_t m_fft_output_f32[NFFT_SAMPLES];      //!< FFT output data. Frequency domain.
static uint8_t m_check_imped_chnl;
#endif

/* -------------------------------------------------------------------
 * Declare State buffer of size (numTaps + blockSize - 1)
 * ------------------------------------------------------------------- */
static q31_t firStateQ31[ADS1299_MAX_NUM_CHNLS][ADS1299_BLOCK_SIZE + ECG_FILT_NUM_TAPS - 1];
/* ----------------------------------------------------------------------
** FIR Coefficients buffer generated using fir1() MATLAB function.
** signal.firwin(79, cutoff = 2/50, window = "hanning", pass_zero=False) # 2 Hz
** ------------------------------------------------------------------- */
q31_t firCoeffsq31[ECG_FILT_NUM_TAPS];

static arm_fir_instance_q31 S[ADS1299_MAX_NUM_CHNLS];
static q31_t biop_raw_buf[ADS1299_MAX_NUM_CHNLS][ADS1299_BLOCK_SIZE];
static uint8_t biop_raw_buf_index = 0;
static q31_t biop_filtered_buf[ADS1299_MAX_NUM_CHNLS][ADS1299_BLOCK_SIZE];

/** Forward Declaration */
static void enable_modules(void);
static void disable_modules(void);
static void sensor_init(void);
static void sensor_uninit(void);
static void start_sensor_modules(void);
static void stop_sensor_modules(void);
static void advertising_start(bool erase_bonds);
static void biop_data_process(void *p_event_data, uint16_t event_size);
static void nand_data_process(void *p_event_data, uint16_t event_size);
static void dl_pages_reset(void);
static void update_conn_interval(void *p_event_data, uint16_t event_size);
static void advertising_init(void);
static int8_t imu_start(void);
static int8_t imu_stop(void);
static int8_t imu_module_init(void);
static int8_t imu_reset_fifo(void);

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

#ifdef USE_FFT
/**
 * @brief Function for processing generated sine wave samples.
 * @param[in] p_input        Pointer to input data array with complex number samples in time domain.
 * @param[in] p_input_struct Pointer to cfft instance structure describing input data.
 * @param[out] p_output      Pointer to processed data (bins) array in frequency domain.
 * @param[in] output_size    Processed data array size.
 */
static void fft_process(float32_t *                   p_input,
                        const arm_cfft_instance_f32 * p_input_struct,
                        float32_t *                   p_output,
                        uint16_t                      output_size)
{
    // Use CFFT module to process the data.
    arm_cfft_f32(p_input_struct, p_input, m_ifft_flag, m_do_bit_reverse);
    // Calculate the magnitude at each bin using Complex Magnitude Module function.
    arm_cmplx_mag_f32(p_input, p_output, output_size);
}

static void biop_calc_impedance(ads1299_dev *dev)
{
    int32_t cur_val = biop_raw_buf[m_check_imped_chnl][biop_raw_buf_index];

    if (m_fft_count < NFFT_SAMPLES)
    {
        // Real part.
        m_fft_input_f32[m_fft_count*2] = (float)cur_val * ADS1299_LSB;
        // Img part.
        m_fft_input_f32[m_fft_count*2 + 1] = 0;

        m_fft_count++;
    }
    else if (m_fft_count == NFFT_SAMPLES)
    {
        //collected all samples for this channel, do FFT
        fft_process(m_fft_input_f32, &arm_cfft_sR_f32_len1024, m_fft_output_f32, NFFT_SAMPLES);

        //FDR/4/(FDR/NFFT_SAMPLES) = NFFT_SAMPLES/4
        float voltage = m_fft_output_f32[NFFT_SAMPLES/4]/NFFT_SAMPLES;
        float impedance = voltage/ADS1299_CURRENT;

//        float act_impedance = 19.554721f*impedance*impedance - 26669.083224f*impedance + 9095712.168381f;  //calib: 2.7k -82k
//        float act_impedance = 7.675000f*impedance*impedance - 10058.233483f*impedance + 3292091.813342f;  //calib: 1.2k - 15k
        float act_impedance = -14.099341f*impedance*impedance + 14254.605605f*impedance - 3586391.607794f;  //calib: 1.2k - 15k

        m_chnl_imped[m_check_imped_chnl] = act_impedance;
//        m_chnl_imped[m_check_imped_chnl] = m_chnl_imped[m_check_imped_chnl] * 0.9f +
//                                     (1.0f - 0.9f) * act_impedance;

        //collect for next channel
        m_fft_count = 0;
        m_check_imped_chnl++;
    }

    //if have performed fft for all channels, report impedance values
    if (m_check_imped_chnl == ADS1299_NUM_CHNLS_PER_DEV)
    {
        //restart the impedance check
        m_check_imped_chnl = 0;
    }
}
#endif

static void biop_impedance_stream(void)
{
    biop_imped_down_sample_cnt++;
    if (biop_imped_down_sample_cnt < 50)
        return;

    biop_imped_down_sample_cnt = 0;

    //TODO: expand this function to accommodate additional channels - hack to only stream 8 for now
    for (int j = 0; j < ADS1299_MAX_STREAM_CHNLS; j++) {
#ifndef USE_FFT
        int32_t signed_int = (int32_t)(m_chnl_imped[j]*1000.0f);
#else
        int32_t signed_int = (int32_t)(m_chnl_imped[j]/1000.0f);
#endif

        biop_stream_buf[biop_stream_buf_idx][biop_stream_index++] = (signed_int) & 0xFF;
        biop_stream_buf[biop_stream_buf_idx][biop_stream_index++] = (signed_int >> 8) & 0xFF;
    }

    //duplicate the impedance values and send 
    for (uint8_t j=0; j < 6; j++)
    {
        memcpy(&biop_stream_buf[biop_stream_buf_idx][j*sizeof(uint16_t)*ADS1299_MAX_STREAM_CHNLS], 
                  &biop_stream_buf[biop_stream_buf_idx][0], sizeof(uint16_t)*ADS1299_MAX_STREAM_CHNLS);
    }

    app_sched_event_put(&biop_stream_buf_idx, sizeof(uint8_t), biop_data_process);
    biop_stream_index = 0;
    biop_stream_buf_idx++;
    biop_stream_buf_idx %= 2;
}

static void biop_voltage_stream(void)
{
    int32_t cur_val;

    biop_down_sample_cnt++;
    if (biop_down_sample_cnt < 10)
        return;

    biop_down_sample_cnt = 0;

    // Increment Index
    biop_raw_buf_index += 1;

    // Filter next block if needed
    if (biop_raw_buf_index == ADS1299_BLOCK_SIZE) {
        biop_raw_buf_index = 0;

        for (uint8_t i = 0; i < 4; i++) {
            //memcpy(biop_filtered_buf[i], biop_raw_buf[i], ADS1299_BLOCK_SIZE*sizeof(q31_t));
            arm_fir_q31(&S[i], biop_raw_buf[i], biop_filtered_buf[i], ADS1299_BLOCK_SIZE); 
        }
        for (uint8_t i = 8; i < 12; i++) {
            //memcpy(biop_filtered_buf[i], biop_raw_buf[i], ADS1299_BLOCK_SIZE*sizeof(q31_t));
            arm_fir_q31(&S[i], biop_raw_buf[i], biop_filtered_buf[i], ADS1299_BLOCK_SIZE); 
        }
        
    }

    //TODO: expand this function to accommodate additional channels - hack to only stream 8 for now
    // Package for streaming
    for (int i = 0; i < ADS1299_MAX_STREAM_CHNLS; i++) {
        if (i < 4)
            cur_val = biop_filtered_buf[i][biop_raw_buf_index];
        else
            cur_val = biop_filtered_buf[i+4][biop_raw_buf_index];
        // int32_t stream_volt_val = (int32_t)((float)cur_val * ADS1299_LSB * 1000.0f);
        
        if(cur_val < -32768)
        {
            cur_val = -32768;
        } else if(cur_val > 32767)
        {
            cur_val = 32767;
        } 

        biop_stream_buf[biop_stream_buf_idx][biop_stream_index++] = cur_val & 0xFF;
        biop_stream_buf[biop_stream_buf_idx][biop_stream_index++] = (cur_val >> 8) & 0xFF;

    }

    if (biop_stream_index == BIOP_BUF_SIZE) {
        app_sched_event_put(&biop_stream_buf_idx, sizeof(uint8_t), biop_data_process);
        biop_stream_index = 0;
        biop_stream_buf_idx++;
        biop_stream_buf_idx %= 2;
    }
}

#ifndef USE_FFT
//adapted from: https://github.com/peabody124/TauLabs/blob/eeg/flight/Modules/EEG/eeg.c
static void biop_calc_impedance(ads1299_dev *dev)
{
    // Measure the power at fDR / 4
    // Sampling rate at 500 sps, impedance pulse at 1/4 that and
    // then impedance measured at 1/8 that (so updated at 15 Hz)
    const uint32_t PERIODS = 8;
    const float IMPEDANCE_ALPHA = 0.97f;  // IMPEDANCE_ALPHA of 0.97 gives about one second time constant
    m_ads1299_sample++;

    for (uint32_t i = 0; i < dev->num_chnls; i++) {
        int32_t cur_val = biop_filtered_buf[i][biop_raw_buf_index];
        float chnl_voltage = (float)cur_val * ADS1299_LSB;

        cos_accum[i] += chnl_voltage * cos_table[m_ads1299_sample % 4];
        sin_accum[i] += chnl_voltage * sin_table[m_ads1299_sample % 4];
    }

    if (m_ads1299_sample % (4 * PERIODS) == 0) {
            
        for (uint32_t i = 0; i < dev->num_chnls; i++) {

            float a = cos_accum[i] / 2.0f / PERIODS;
            float b = sin_accum[i] / 2.0f / PERIODS;
            float amp = sqrtf(a*a + b*b); //uV

            // Fudge factor. This is a square wave pulse and we are measuring
            // the amplitude of the base component. This compensates.
            amp *= 0.7071f; // sqrt(2)

#ifndef SEND_IMPED_VOLTAGES
            // +/- 6nA square wave pulse. Convert voltage to nV
#ifdef LOFF_TEST_6NA
            float impedance =  amp * 1000.0f / 6.0f;
#else
            float impedance =  amp * 10.0f / 24.0f;
#endif

            m_chnl_imped[i] = m_chnl_imped[i] * IMPEDANCE_ALPHA +
                                     (1 - IMPEDANCE_ALPHA) * impedance;
#else
            // +/- 6nA square wave pulse. Convert voltage to nV
            float impedance =  amp;  //voltage only

            m_chnl_imped[i] = m_chnl_imped[i] * IMPEDANCE_ALPHA +
                                     (1 - IMPEDANCE_ALPHA) * impedance;
#endif

            // Reset the accumulator
            cos_accum[i] = 0;
            sin_accum[i] = 0;
        }
    }
}
#endif

static void biop_data_save(ads1299_packed_data *data, uint32_t t_ms, ads1299_dev *dev)
{
    //TODO: expand this function to accommodate additional channels
#ifdef SAVE_IMPEDANCE_VALS
    for (uint8_t i=0; i < ADS1299_NUM_CHNLS_PER_DEV; i++)
    {
        int32_t signed_int = (int32_t)(m_chnl_imped[i]*1000);

        *(biop_nand_pages[biop_page_index].section.body+biop_buf_index++) = (signed_int >> 16) & 0xFF;
        *(biop_nand_pages[biop_page_index].section.body+biop_buf_index++) = (signed_int >> 8) & 0xFF;
        *(biop_nand_pages[biop_page_index].section.body+biop_buf_index++) = (signed_int) & 0xFF;
    }
#else
    memcpy(biop_nand_pages[biop_page_index].section.body+biop_buf_index, ADS1299_CHNL_DATA_PTR(data, 0), 
              ADS1299_BYTES_PER_SAMPLE*8); 
    memcpy(biop_nand_pages[biop_page_index].section.body+biop_buf_index +ADS1299_BYTES_PER_SAMPLE*8, ADS1299_CHNL_DATA_PTR(data, 8), 
              ADS1299_BYTES_PER_SAMPLE*8); 
    biop_buf_index += dev->num_chnls*ADS1299_BYTES_PER_SAMPLE;
#endif

    if (biop_buf_index < 2016)
        return;

    biop_buf_index = 0;
    memory_evt_t page_evt;

    if(t_ms == TS_INVALID_TIME) {
        NRF_LOG_INFO("Time Stamp Invalid")
        return;
    }

    biop_nand_pages[biop_page_index].section.header.data_type = SH_DATA_TYPE_BIOP_ADS1299_MODE_2;
    biop_nand_pages[biop_page_index].section.header.flag = 0;
    biop_nand_pages[biop_page_index].section.header.timestamp_ms = t_ms;
    biop_nand_pages[biop_page_index].section.header.data_len = 42;
    page_evt.page = &biop_nand_pages[biop_page_index];
//    NRF_LOG_INFO("Writing to BIOP to flash index %d, Time %d",biop_page_index, t_ms/8169);
    
    // Change page index
    if (biop_page_index == 1) {
        biop_page_index = 0;
    } else {
        biop_page_index = 1;
    }

    app_sched_event_put(&page_evt, sizeof(page_evt), nand_data_process);
}

static void ads1299_callback(uint8_t *data, ads1299_dev *dev)
{
    uint32_t t_ms = 0;
    int32_t cur_val;
    ads1299_packed_data *chnl_data = (ads1299_packed_data *)data;

    ads1299_wdt = 0;

     // Get Time Stamp if neccessary
    if (biop_buf_index + ADS1299_BYTES_PER_SAMPLE*dev->num_chnls >= 2016) {
        t_ms = ts_timestamp_get_ticks_u32(5);
    }

    // Package data to be filtered
    for (uint8_t i = 0; i < dev->num_chnls; i++) {
        cur_val = (*(ADS1299_CHNL_DATA_PTR(chnl_data, i)) << 16) | 
                (*(ADS1299_CHNL_DATA_PTR(chnl_data, i) + 1) << 8) | *(ADS1299_CHNL_DATA_PTR(chnl_data, i) + 2);

        // check if the value is negative
        if (*(ADS1299_CHNL_DATA_PTR(chnl_data, i)) & 0x80) { 
            cur_val |=  0xFF000000;
        }

        biop_raw_buf[i][biop_raw_buf_index] = cur_val;
    }

    if (dev->is_imped_check_active) {
        biop_calc_impedance(dev);
    }

    // Stream Data if enabled
    if (m_ble_stream_biop) {
        if (dev->is_imped_check_active) {
            biop_impedance_stream();
        } else {
            biop_voltage_stream();
        }
    }

    if (m_active_session) {
        biop_data_save(chnl_data, t_ms, dev);
    }
}

static int8_t biop_init_devs(void)
{
    int8_t err_code;

    for (uint8_t i=0; i < ADS1299_NUM_DAISY_CHAINS; i++)
    {
        memset(&m_ads1299_dev[i], 0, sizeof(m_ads1299_dev[i]));
        m_ads1299_dev[i].continuous_reading = false;
        m_ads1299_dev[i].is_imped_check_active = false;

        // Hardware configuraiton
        m_ads1299_dev[i].num_daisy_chn_devs = ADS1299_NUM_DEVS_PER_DAISY_CHN;
        m_ads1299_dev[i].num_chnls = ADS1299_NUM_DEVS_PER_DAISY_CHN * ADS1299_NUM_CHNLS_PER_DEV;
        m_ads1299_dev[i].reset_pin = ADS1299_RESET_PIN;
        m_ads1299_dev[i].pwdn_pins[0] = ADS1299_1_PWDN_PIN;
        m_ads1299_dev[i].pwdn_pins[1] = ADS1299_2_PWDN_PIN;
        m_ads1299_dev[i].start_pin = ADS1299_START_PIN;
        m_ads1299_dev[i].drdy_pin = ADS1299_1_DRDY_PIN;

        // Device comm
        m_ads1299_dev[i].read = ads1299_spi_read;
        m_ads1299_dev[i].write = ads1299_spi_write;
        m_ads1299_dev[i].cont_read_enable = ads1299_spi_cont_read_enable;

        // Delay
        m_ads1299_dev[i].delay_ms = nrf_delay_ms;

        // GPIO
        m_ads1299_dev[i].reset = ads1299_gpio_reset;
        m_ads1299_dev[i].start = ads1299_gpio_start;
        m_ads1299_dev[i].pwdn = ads1299_gpio_pwdn;

        // Callback for continuous read data
        m_ads1299_dev[i].data_callback = ads1299_callback;

        // CONFIG1: Set sampling rate (ADS1299_CONFIG1_SETUP_1000)
        m_ads1299_dev[i].config1.bit.reserved2 = 0x01;                      //always write 1h
        m_ads1299_dev[i].config1.bit.daisy_en = 0x00;                       //daisy chain enabled (multiple readback disabled)
        m_ads1299_dev[i].config1.bit.clk_en = 0x01;                         //oscillator clock output enabled
        m_ads1299_dev[i].config1.bit.reserved1 = 0x02;                      //always write 2h
        m_ads1299_dev[i].config1.bit.data_rate = ADS1299_OUTPUT_DR_1K_SPS;  //1000 samples/sec


        /*
        // CONFIG2: No test signal (ADS1299_CONFIG2_SETUP_TEST_EXT)
        m_ads1299_dev[i].config2.bit.reserved2 = 0x06;       //always write 6h
        m_ads1299_dev[i].config2.bit.int_cal = 0x01;         //test signals are generated Internally
        m_ads1299_dev[i].config2.bit.reserved1 = 0x00;       //always write 0h
        m_ads1299_dev[i].config2.bit.cal_amp = 0x01;         //cal signal amplitude: 0 : 1  (VREFP  VREFN) / 2400 OR 1 : 2  (VREFP  VREFN) / 2400
        m_ads1299_dev[i].config2.bit.cal_freq = 0x01;        //cal signal freq: Pulsed at fCLK / 2^21
        */
        m_ads1299_dev[i].config2.data = 0xD0;


        // CONFIG3: Bias - BIAS sense function is not enabled
        //(ADS1299_CONFIG3_SETUP_PWR_REF | ADS1299_CONFIG3_SETUP_RESERVED |
        //ADS1299_CONFIG3_SETUP_BIAS_MEAS | ADS1299_CONFIG3_SETUP_BIAS_INT |
        //ADS1299_CONFIG3_SETUP_PWR_BIAS = 0xFC)

        /*
        m_ads1299_dev[i].config3.bit.pd_ref_buf = 0x01;      //enable internal reference buffer
        m_ads1299_dev[i].config3.bit.reserved = 0x03;        //always write 3h
        m_ads1299_dev[i].config3.bit.bias_meas = 0x00;       //BIAS_IN signal is routed to the channel that has the MUX_Setting 010 (VREF)
        m_ads1299_dev[i].config3.bit.biasref_int = 0x01;     //BIASREF signal (AVDD + AVSS) / 2 generated internally
        m_ads1299_dev[i].config3.bit.pd_bias = 0x00;         //BIAS buffer is disabled
        m_ads1299_dev[i].config3.bit.bias_loff_sens = 0x00;  //BIAS sense is disabled
        m_ads1299_dev[i].config3.bit.bias_stat = 0x00;       //Read-only
        */
        m_ads1299_dev[i].config3.data = 0xEC;


        // CONFIG4: Set defaults
        m_ads1299_dev[i].config4.bit.reserved3 = 0x00;      //always write 0h
        m_ads1299_dev[i].config4.bit.single_shot = 0x00;    //continuous conversion mode
        m_ads1299_dev[i].config4.bit.reserved2 = 0x00;      //always write 0h
        m_ads1299_dev[i].config4.bit.pd_loff_comp = 0x00;   //lead-off comparators disabled
        m_ads1299_dev[i].config4.bit.reserved1 = 0x00;      //always write 0h


        /*
        // Enable all channels
        m_ads1299_dev[i].ch1set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        m_ads1299_dev[i].ch2set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        m_ads1299_dev[i].ch3set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        m_ads1299_dev[i].ch4set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        m_ads1299_dev[i].ch5set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        m_ads1299_dev[i].ch6set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        m_ads1299_dev[i].ch7set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        m_ads1299_dev[i].ch8set.data = ADS1299_CH_N_SET_SETUP_GAIN_24 | ADS1299_CH_N_SET_SETUP_MUX_NEI;
        */

        m_ads1299_dev[i].ch1set.data = 0x60; // Acquire test signal
        m_ads1299_dev[i].ch2set.data = 0x60; // Acuire BIASIN with this channel
        m_ads1299_dev[i].ch3set.data = 0x60; // Acuire electrode
        m_ads1299_dev[i].ch4set.data = 0x60; // Acuire electrode
        m_ads1299_dev[i].ch5set.data = 0x60; // Acuire electrode
        m_ads1299_dev[i].ch6set.data = 0x60; // Acuire electrode
        m_ads1299_dev[i].ch7set.data = 0x60; // Acuire electrode
        m_ads1299_dev[i].ch8set.data = 0x60; // Acuire electrode


        // Settings for lead-off detection
        m_ads1299_dev[i].loff.bit.comp_th = 0x00;      //lead-off comparator threshold 95% pos/5% neg
        m_ads1299_dev[i].loff.bit.reserved1 = 0x00;    //always write 0h
#ifdef LOFF_TEST_6NA
        m_ads1299_dev[i].loff.bit.ilead_off = 0x00;    //lead-off current magnitude 6nA
#else
        m_ads1299_dev[i].loff.bit.ilead_off = 0x03;    //lead-off current magnitude 24uA
#endif
        m_ads1299_dev[i].loff.bit.flead_off = 0x03;    //lead-off frequency fDR/4 square pulse

        // Settings for disabling lead-off detection
        m_ads1299_dev[i].loffsensp.data = 0x00;                   // Test no leads
        m_ads1299_dev[i].loffsensn.data = 0x00;

        // Settings for enabling impedence checking
        m_ads1299_dev[i].loffsensp_imped_test.data = 0xFF;        // Test all leads
        m_ads1299_dev[i].loffsensn_imped_test.data = 0xFF;

        // Enable bias circuitry and give feedback based on all the channels
        m_ads1299_dev[i].biassensp.data = 0xFF; // Measure bias from only channel 1 (Ch 1 is the only channel getting the test signal)
        m_ads1299_dev[i].biassensn.data = 0x0; // Measure bias from negative leads

        // Turn off bias for impedance checking
        m_ads1299_dev[i].biassensp_imped_test.data = 0x00; // Turn bias off from all leads
        m_ads1299_dev[i].biassensn_imped_test.data = 0x00; // Turn bias off from all negative leads

        // Measure bias from negative leads
        m_ads1299_dev[i].misc1.data = ADS1299_MISC1_SETUP_SRB1; 

        NRF_LOG_INFO("config1: 0x%x", m_ads1299_dev[i].config1.data);
        NRF_LOG_INFO("config2: 0x%x", m_ads1299_dev[i].config2.data);
        NRF_LOG_INFO("config3: 0x%x", m_ads1299_dev[i].config3.data);
        NRF_LOG_INFO("config4: 0x%x", m_ads1299_dev[i].config4.data);
        NRF_LOG_INFO("ch1set: 0x%x", m_ads1299_dev[i].ch1set.data);
        NRF_LOG_INFO("ch2set: 0x%x", m_ads1299_dev[i].ch2set.data);
        NRF_LOG_INFO("ch3set: 0x%x", m_ads1299_dev[i].ch3set.data);
        NRF_LOG_INFO("ch4set: 0x%x", m_ads1299_dev[i].ch4set.data);
        NRF_LOG_INFO("ch5set: 0x%x", m_ads1299_dev[i].ch5set.data);
        NRF_LOG_INFO("ch6set: 0x%x", m_ads1299_dev[i].ch6set.data);
        NRF_LOG_INFO("ch7set: 0x%x", m_ads1299_dev[i].ch7set.data);
        NRF_LOG_INFO("ch8set: 0x%x", m_ads1299_dev[i].ch8set.data);
        NRF_LOG_INFO("loff: 0x%x", m_ads1299_dev[i].loff.data);
        NRF_LOG_INFO("loffsensp: 0x%x", m_ads1299_dev[i].loffsensp.data);
        NRF_LOG_INFO("loffsensn: 0x%x", m_ads1299_dev[i].loffsensn.data);
        NRF_LOG_INFO("biassensp: 0x%x", m_ads1299_dev[i].biassensp.data);
        NRF_LOG_INFO("biassensn: 0x%x", m_ads1299_dev[i].biassensn.data);
        NRF_LOG_INFO("misc1: 0x%x", m_ads1299_dev[i].misc1.data);
        NRF_LOG_FLUSH();

        err_code = ads1299_init(&m_ads1299_dev[i]);

        if (err_code != ADS1299_OK) {
          sys_status.fetal.ads1299 = MODULE_FAIL;
          NRF_LOG_INFO("ADS1299 %d Init Failed", i);
          break;
        }
        else {
          sys_status.fetal.ads1299 = MODULE_SUCCESS;
          NRF_LOG_INFO("ADS1299 %d Init Success", i);
          ads1299_start_cont_read(&m_ads1299_dev[i]);
        }
    }

    return err_code;
}

static void biop_uninit_devs(void)
{
    for (uint8_t i=0; i < ADS1299_NUM_DAISY_CHAINS; i++)
    {
        ads1299_stop_cont_read(&m_ads1299_dev[i]);
        ads1299_uninit(&m_ads1299_dev[i]);
    }
}

static void module_wdt_handler(void * p_context)
{
    ret_code_t err_code;

    nrfx_wdt_channel_feed(m_channel_id);

    if(m_modules_initalized == false) {
        return;
    }

    // Increment the wdt counters
    ads1299_wdt++;

    if(ads1299_wdt > WDT_CNT_LIMIT) {
      NRF_LOG_INFO("ADS1299 WDT Limit Reached. Restarting Module");

      err_code = biop_init_devs();
      
      if (err_code == ADS1299_OK) {
        ads1299_wdt = 0;
        biop_buf_index = 0;

        NRF_LOG_INFO("ADS1299 Reinitalized Success");
      }
    }

}

static void enter_critical_battery_level(void) {

    ret_code_t rc;


    if(m_active_session) {

        ret_code_t rc = sh_dl_session_stop();
        if (rc != SH_DL_INVALID_STATE && rc != SH_DL_SUCCESS) {
          NRF_LOG_INFO("SH_DL Stop Session")
        }

        ts_disable();
        m_active_session = false;
    }
 
    sensor_uninit();
    sh_dl_abort_download();

    NRF_LOG_INFO("Enter Crtical Battery Level Mode");
    NRF_LOG_FLUSH();

}

static bool is_bat_critical(void) {
    nrf_gpio_pin_set(BQ_CD_PIN);
    nrf_delay_ms(BQ_ENABLE_DELAY_MS);
    bq25120_get_batt_range(true, &pmic_dev);
    nrf_gpio_pin_clear(BQ_CD_PIN);

    return (pmic_dev.batt_range < 86);
}

static void bat_crit_check_timeout_hndlr(void *p_context) {
}

static void read_pmic_status() {
    /** Read PMIC Status */
    nrf_gpio_pin_set(BQ_CD_PIN);
    nrf_delay_ms(BQ_ENABLE_DELAY_MS);
    bq25120_read_status(&pmic_dev);
    bq25120_get_batt_range(true, &pmic_dev);
    nrf_gpio_pin_clear(BQ_CD_PIN);

    if( pmic_dev.status.charge_sta != BQ25120_ChargeStatus_Ready ) {
        battery_info.charging_status = 1;
        nrf_gpio_pin_toggle(GREEN_INDICATION_LED_PIN);
    } else {
        battery_info.charging_status = 0;
        nrf_gpio_pin_clear(GREEN_INDICATION_LED_PIN);
    }
    battery_info.charging_status = 1;
    // Enter & Exit Crticial Battery Level
    if(pmic_dev.batt_range <= 82) {
        enter_critical_battery_level();
        nrf_gpio_pin_set(BQ_CD_PIN);
        bq25120_shipmode_fun(Disabled,&pmic_dev);
        bq25120_shipmode_fun(Enabled,&pmic_dev);
        nrf_gpio_pin_clear(BQ_CD_PIN);
    }

    if(pmic_dev.batt_range >= 98) {
        battery_info.battery_level = 100;
    } else if(pmic_dev.batt_range >= 96) {
        battery_info.battery_level = 80;
    } else if(pmic_dev.batt_range >= 94) {
        battery_info.battery_level = 60;
    } else if(pmic_dev.batt_range >= 90) {
        battery_info.battery_level = 40;
    } else if(pmic_dev.batt_range >= 86) {
        battery_info.battery_level = 20;
    } else {
        battery_info.battery_level = 0;
        nrfx_wdt_channel_feed(m_channel_id);
    }

//    NRF_LOG_INFO("Battery Status Charging: %d Level: %d", battery_info.charging_status,battery_info.battery_level);

}

static void read_pmic(void *p_event_data, uint16_t event_size) {

    if( battery_info.charging_status == 1 || pmic_read_cnt++ >= PMIC_NON_CHARGE_READ_SEC || interrupt_flag) {
        read_pmic_status();
        pmic_read_cnt = 0;
        interrupt_flag = false;
        ble_bas_battery_level_update(&m_bas, battery_info.data, BLE_CONN_HANDLE_ALL);
    }

}

static void pmic_status_timeout_handler(void *p_context) {

  UNUSED_PARAMETER(p_context);

//  if(m_active_session) {
      app_sched_event_put(NULL, 0, read_pmic);
//  } else {
//      read_pmic(NULL, 0);
//  }

}

static void biop_data_process(void *p_event_data, uint16_t event_size) {

    if(app_sched_p5_flag == true) {
      return;
    }
    UNUSED_PARAMETER(event_size);
    uint8_t stream_buf_idx = *(uint8_t*)p_event_data;
    ret_code_t err = ble_biop_meas_send(&m_ble_biop, &biop_stream_buf[stream_buf_idx][0], BIOP_BUF_SIZE);

    if (err != NRF_SUCCESS){
        NRF_LOG_INFO("BIOP_DATA_PROCESS Failed: %d", err);
    }
    //NRF_LOG_INFO("BIOP data processed: %d", stream_buf_idx);
}


static void nand_data_process(void *p_event_data, uint16_t event_size) {
  UNUSED_PARAMETER(event_size);
  ret_code_t rc;

  memory_evt_t *p_memory_evt_t = (memory_evt_t *)p_event_data;
  rc = sh_dl_session_write_page(p_memory_evt_t->page);
  if( rc != SH_DL_SUCCESS){
      NRF_LOG_INFO("NAND_DATA_PROCESS Failed: %d",rc);
  }
  //NRF_LOG_INFO("NAND data processed");
}

static void session_end_callback(void *p_event_data, uint16_t event_size) {
    ret_code_t rc;

    if(m_active_session) {

        ret_code_t rc = sh_dl_session_stop();
        if (rc != SH_DL_INVALID_STATE && rc != SH_DL_SUCCESS) {
          NRF_LOG_INFO("SH_DL Stop Session")
        }

        ts_disable();
        m_active_session = false;
    }

    sensor_uninit();

    NRF_LOG_INFO("End Session and Module");
    NRF_LOG_FLUSH();
}

static void session_end_handler(void * p_context)
{
    NRF_LOG_INFO("END Session");
    app_sched_event_put(NULL, 0, session_end_callback);
}

static void twi_mngr_wait(void)
{
   sd_app_evt_wait();
}

int8_t pmic_twi_write(uint8_t id, uint8_t reg_addr, uint8_t *aux_data, uint16_t len)
{
    twi_buf[0] =  reg_addr;
    for(int i = 0; i < len; i ++) {
    twi_buf[i+1] = aux_data[i];
    }

    nrf_twi_mngr_transfer_t const transaction[] =
    {
        NRF_TWI_MNGR_WRITE(id, &twi_buf, len+1, 0),
    };

    return nrf_twi_mngr_perform(&m_nrf_twi_mngr, NULL, transaction, 1, twi_mngr_wait);

}

/*wrapper function to match the signature of bmm150.read */
int8_t pmic_twi_read(uint8_t id, uint8_t reg_addr, uint8_t *aux_data, uint16_t len)
{

    nrf_twi_mngr_transfer_t const transaction[] =
    {
        NRF_TWI_MNGR_WRITE(id, &reg_addr, 1, NRF_TWI_MNGR_NO_STOP),
        NRF_TWI_MNGR_READ(id, aux_data,   len, 0)
    };

    return nrf_twi_mngr_perform(&m_nrf_twi_mngr, NULL,transaction, 2, twi_mngr_wait);

}

static void indicator_led_blink(uint16_t duration_ms) {
    nrf_gpio_pin_set(GREEN_INDICATION_LED_PIN);
    nrf_delay_ms(duration_ms);
    nrf_gpio_pin_clear(GREEN_INDICATION_LED_PIN);
}

/**@brief Clear bond information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for advertising wrapper.
 */
static void advertising_start_handler(void *p_event_data, uint16_t event_size) {
  advertising_start(false);
}

/**@brief Function for starting advertising.
 */
void advertising_start(bool erase_bonds)
{
    if (erase_bonds == true)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETE_SUCCEEDED event.
    }
    else
    {

//        if(m_active_session == false && battery_info.charging_status == false) {
        if(m_active_session == false) {
            indicator_led_blink(8);
        }

        ret_code_t err_code;
        advertising_init();
        err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
        APP_ERROR_CHECK(err_code);

        nrfx_wdt_channel_feed(m_channel_id);
    }
}


/**@brief Function for handling File Data Storage events.
 *
 * @param[in] p_evt  Peer Manager event.
 * @param[in] cmd
 */
static void fds_evt_handler(fds_evt_t const * const p_evt)
{
    if (p_evt->id == FDS_EVT_GC)
    {
        NRF_LOG_DEBUG("GC completed\n");
    }
}


/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    ret_code_t err_code;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            NRF_LOG_INFO("Connected to a previously bonded device.");
        } break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
            NRF_LOG_INFO("Connection secured: role: %d, conn_handle: 0x%x, procedure: %d.",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);
        } break;

        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
        } break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;

        case PM_EVT_STORAGE_FULL:
        {
            // Run garbage collection on the flash.
            err_code = fds_gc();
            if (err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(err_code);
            }
        } break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        {
            NRF_LOG_DEBUG("PM_EVT_PEERS_DELETE_SUCCEEDED");
            advertising_start(false);
        } break;

        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } break;

        case PM_EVT_PEER_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } break;

        case PM_EVT_PEERS_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        } break;

        case PM_EVT_ERROR_UNEXPECTED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } break;

        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
            // This can happen when the local DB has changed.
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}

static void buttonless_dfu_sdh_state_observer(nrf_sdh_state_evt_t state, void * p_context)
{
    if (state == NRF_SDH_EVT_STATE_DISABLED)
    {
        // Softdevice was disabled before going into reset. Inform bootloader to skip CRC on next boot.
        nrf_power_gpregret2_set(BOOTLOADER_DFU_SKIP_CRC);

        //Go to system off.
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
    }
}

/* nrf_sdh state observer. */
NRF_SDH_STATE_OBSERVER(m_buttonless_dfu_state_obs, 0) =
{
    .handler = buttonless_dfu_sdh_state_observer,
};

static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
            NRF_LOG_INFO("Device is preparing to enter bootloader mode.");
            // YOUR_JOB: Disconnect all bonded devices that currently are connected.
            //           This is required to receive a service changed indication
            //           on bootup after a successful (or aborted) Device Firmware Update.
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            NRF_LOG_INFO("Device will enter bootloader mode.");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("Request to enter bootloader mode failed asynchroneously.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.nand_data_process
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("Request to send a response to client failed.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
            break;
    }
}
/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_HEART_RATE_SENSOR_HEART_RATE_BELT);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief GATT module event handler.
 */
static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        NRF_LOG_INFO("GATT ATT MTU on connection 0x%x changed to %d.",
                     p_evt->conn_handle,
                     p_evt->params.att_mtu_effective);
    }

}


/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);
  
    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, APP_MTU_SIZE);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_data_length_set(&m_gatt, BLE_CONN_HANDLE_INVALID, APP_DLE_SIZE);
    APP_ERROR_CHECK(err_code);   
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

void ble_biop_evt_handler(ble_biop_t * p_ble_biop, ble_biop_evt_t * p_evt, ble_biop_notification_type_t notification_type)
{
    if(p_evt->evt_type  == BLE_BIOP_EVT_NOTIFICATION_ENABLED) {
      if(notification_type == BLE_BIOP_EVT_NOTIFICATION_STREAM) {

        ble_gap_conn_params_t params;
        params.min_conn_interval = MIN_STREAM_CONN_INTERVAL_MS; /**< Minimum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
        params.max_conn_interval = MAX_STREAM_CONN_INTERVAL_MS; /**< Maximum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
        params.slave_latency = SLAVE_LATENCY;         /**< Slave Latency in number of connection events, see @ref BLE_GAP_CP_LIMITS.*/
        params.conn_sup_timeout = CONN_SUP_TIMEOUT;   /**< Connection Supervision Timeout in 10 ms units, see @ref BLE_GAP_CP_LIMITS.*/

        app_sched_event_put(&params, sizeof(params), update_conn_interval);

        m_ble_stream_biop = true;
        biop_stream_index = 0;
        biop_stream_buf_idx = 0;
      }
    } else if(p_evt->evt_type  == BLE_BIOP_EVT_NOTIFICATION_DISABLED) {
        ble_gap_conn_params_t params;
        params.min_conn_interval = MIN_CONN_INTERVAL; /**< Minimum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
        params.max_conn_interval = MAX_CONN_INTERVAL; /**< Maximum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
        params.slave_latency = SLAVE_LATENCY;         /**< Slave Latency in number of connection events, see @ref BLE_GAP_CP_LIMITS.*/
        params.conn_sup_timeout = CONN_SUP_TIMEOUT;   /**< Connection Supervision Timeout in 10 ms units, see @ref BLE_GAP_CP_LIMITS.*/
        m_ble_stream_biop = false;
        app_sched_event_put(&params, sizeof(params), update_conn_interval);
    }
}

void ble_biop_cmd_handler(ble_biop_t * p_biop, uint8_t const * p_data, uint16_t length)
{
    uint8_t cmd = *p_data;

    switch (cmd)
    {
        case BLE_BIOP_CMD_START_IMP_CHECK:
        {
            ads1299_stop_cont_read(&m_ads1299_dev[0]);
            m_ads1299_sample = 0;
            memset(&cos_accum[0], 0, sizeof(cos_accum));
            memset(&sin_accum[0], 0, sizeof(sin_accum));
            memset(&m_chnl_imped[0], 0, sizeof(m_chnl_imped));
            ads1299_enable_impedance(true, &m_ads1299_dev[0]);
            ads1299_start_cont_read(&m_ads1299_dev[0]);
        }
        break;

        case BLE_BIOP_CMD_STOP_IMP_CHECK:
        {
            ads1299_stop_cont_read(&m_ads1299_dev[0]);
            ads1299_enable_impedance(false, &m_ads1299_dev[0]);
            ads1299_start_cont_read(&m_ads1299_dev[0]);
        }
        break;

        default:
        {
            NRF_LOG_DEBUG("Received unknown BIOP command: %d", cmd);
        }
        break;
    }
}

/**@brief Function for initializing services that will be used by the application.
 *
 * @details Initialize the Heart Rate, Battery and Device Information services.
 */
static void services_init(void)
{
    ret_code_t         err_code;
    ble_biop_init_t           biop_init = {0};
    ble_bas_init_t            bas_init  = {0};
    ble_sys_init_t            sys_init  = {0};
    ble_dis_init_t            dis_init  = {0};
    ble_dfu_buttonless_init_t dfus_init = {0};
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize the async SVCI interface to bootloader.
    err_code = ble_dfu_buttonless_async_svci_init();
    APP_ERROR_CHECK(err_code);

    dfus_init.evt_handler = ble_dfu_evt_handler;
    err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);

    /* BIOP Service Init */
    memset(&biop_init, 0, sizeof(biop_init));

    biop_init.evt_handler                 = ble_biop_evt_handler;
    biop_init.cmd_handler                 = ble_biop_cmd_handler;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&biop_init.biop_meas_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&biop_init.biop_meas_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&biop_init.biop_meas_attr_md.write_perm);

    err_code = ble_biop_init(&m_ble_biop, &biop_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Battery Service.
    memset(&bas_init, 0, sizeof(bas_init));

    // Here the sec level for the Battery Service can be changed/increased.
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&bas_init.battery_level_char_attr_md.write_perm);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_report_read_perm);

    bas_init.evt_handler = NULL;
    bas_init.support_notification = true;
    bas_init.p_report_ref = NULL;

    bas_init.initial_batt_level = 100;

    err_code = ble_bas_init(&m_bas, &bas_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Device Information Service.
    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, (char *)MANUFACTURER_NAME);
    ble_srv_ascii_to_utf8(&dis_init.hw_rev_str, (char *)HW_VER);
    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str, (char *)FW_VER);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);

    /* SYSTEM Service Init */
    memset(&sys_init, 0, sizeof(sys_init));

    sys_init.evt_handler = NULL;

    // Here the sec level for the Heart Rate Service can be changed/increased.
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sys_init.sys_status_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sys_init.sys_status_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&sys_init.sys_status_attr_md.write_perm);

    err_code = ble_sys_init(&m_ble_sys, &sys_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
      APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    // ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
//            NRF_LOG_INFO("Fast advertising.");
            break;

        case BLE_ADV_EVT_IDLE:
            // sleep_mode_enter();
            advertising_start(false);
            break;

        default:
            break;
    }
}

/**@brief Function for handling the Data Logging Service Commands.
 *
 * @details This function will be called for all Custom Service events which are passed to
 *          the application.
 *
 * @param[in]   p_dl_service  Data Logging Service structure.
 * @param[in]   p_evt          Event received from the Data Logging Service.
 *
 */
static void on_dl_cmd(dl_command_t const cmd, uint8_t const *p_param, uint16_t len) {

  switch (cmd) {
  case DL_CMD_START_SESSION: {

        uint8_t rf_address[5] = {0, 0, 0, 0, 0};

        // Reset pages index and buf index
        dl_pages_reset();
        
        if(m_modules_initalized == false) {
            start_sensor_modules();
        }

        if(len != 0) {
            memcpy(rf_address, p_param, sizeof(4)); // Copying start session time
            rf_address[4] = p_param[5];             // Copying frist Byte of session Id
        }

        ts_enable(rf_address);
        ts_reset_curr_pkg();
#if   (TS_TX_MODE)
        ts_tx_start();
#elif (TS_RX_MODE)
        ts_rx_start();
#endif

        m_active_session = true;

        ret_code_t err_code = app_timer_stop(m_session_timer_id);
        if( err_code != NRF_SUCCESS) {
          NRF_LOG_INFO("Restarting: Session app timer stop error: %d", err_code);
        }

        err_code = app_timer_start(m_session_timer_id, SESSION_TIMEOUT, NULL);
        if( err_code != NRF_SUCCESS) {
            NRF_LOG_INFO("Session Timeout app timer error: %d", err_code);
        }

  } break;

  case DL_CMD_STOP_SESSION: {

    ret_code_t err_code = app_timer_stop(m_session_timer_id);
    if( err_code != NRF_SUCCESS) {
      NRF_LOG_INFO("Session app timer stop error: %d", err_code);
    }

    NRF_LOG_INFO("STOP SESSION");
    ts_disable();
    m_active_session = false;
  } break;
  case DL_CMD_START_SESSION_READ: {

    stop_sensor_modules();
    m_active_session = false;

    /* Update BLE connection interval */
    ble_gap_conn_params_t params;
    params.min_conn_interval = MSEC_TO_UNITS(15, UNIT_1_25_MS); /**< Minimum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
    params.max_conn_interval = MSEC_TO_UNITS(15, UNIT_1_25_MS); /**< Maximum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
    params.slave_latency = 3;                                   /**< Slave Latency in number of connection events, see @ref BLE_GAP_CP_LIMITS.*/
    params.conn_sup_timeout = CONN_SUP_TIMEOUT;                 /**< Connection Supervision Timeout in 10 ms units, see @ref BLE_GAP_CP_LIMITS.*/

    uint16_t conn_param_update_cnt = 0;
    while (ble_conn_params_change_conn_params(m_conn_handle, &params) == NRF_ERROR_BUSY && conn_param_update_cnt++ < 300) {
      nrf_delay_ms(100);
      NRF_LOG_INFO("Start Session Read: Waiting for params_change");
      NRF_LOG_FLUSH();
    }
  } break;
  case DL_DONE_SESSION_READ: {

    /* Update BLE connection interval */
    ble_gap_conn_params_t params;
    params.min_conn_interval = MIN_CONN_INTERVAL; /**< Minimum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
    params.max_conn_interval = MAX_CONN_INTERVAL; /**< Maximum Connection Interval in 1.25 ms units, see @ref BLE_GAP_CP_LIMITS.*/
    params.slave_latency = SLAVE_LATENCY;         /**< Slave Latency in number of connection events, see @ref BLE_GAP_CP_LIMITS.*/
    params.conn_sup_timeout = CONN_SUP_TIMEOUT;   /**< Connection Supervision Timeout in 10 ms units, see @ref BLE_GAP_CP_LIMITS.*/

    app_sched_event_put(&params, sizeof(params), update_conn_interval);

    NRF_LOG_INFO("Done downloading");
    NRF_LOG_FLUSH();
  } break;
  case DL_CMD_RESET_DEVICE: {

    nrf_gpio_pin_set(BQ_CD_PIN);
    bq25120_shipmode_fun(Disabled,&pmic_dev);
    bq25120_shipmode_fun(Enabled,&pmic_dev);
    nrf_gpio_pin_clear(BQ_CD_PIN);

  } break;
  case DL_SAVE_DATA: {
    app_sched_p5_flag = true;
    app_sched_execute();
    app_sched_p5_flag = false;
  } break;
  case DL_CMD_RECOVER_MEMORY: {

    // Rename Device name to indicate Recover mode
    char device_name[strlen(DEVICE_NAME) + 3];
    ble_gap_conn_sec_mode_t sec_mode;
    strcpy(device_name,(const char *)DEVICE_NAME);
    strcat(device_name,(const char *)"_MR");

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    sd_ble_gap_device_name_set(&sec_mode,
                                (const uint8_t *)device_name,
                                 strlen(device_name));
  }
  default:
    break;
  }
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:

            NRF_LOG_INFO("Connected.");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);

            sensor_init();
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected, reason %d.",
                          p_ble_evt->evt.gap_evt.params.disconnected.reason);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            sensor_uninit();
            NRF_LOG_INFO("DISCONNECTED");
            app_sched_event_put(NULL,0,advertising_start_handler);

            m_ble_stream_biop = false;
            break;
        case BLE_GAP_EVT_CONN_PARAM_UPDATE:
        {
            ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;
            // Accept parameters requested by the peer.
            ble_gap_conn_params_t params;
            params = p_gap_evt->params.conn_param_update_request.conn_params;
//            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle, &params);
//            APP_ERROR_CHECK(err_code);

            NRF_LOG_INFO("Connection interval updated (upon request): 0x%x, 0x%x.",
                p_gap_evt->params.conn_param_update_request.conn_params.min_conn_interval,
                p_gap_evt->params.conn_param_update_request.conn_params.max_conn_interval);
        } break;
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_2MBPS,
                .tx_phys = BLE_GAP_PHY_2MBPS,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
/**@brief Function for the Peer Manager initialization.
 */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = fds_register(fds_evt_handler);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void) {
  ret_code_t               err_code;
  ble_advdata_manuf_data_t manuf_specific_data;
  ble_advertising_init_t   init;
  sh_dl_memory_t           memory_status;
  /** Get Device Type */
  m_adv_data.device_type = device_type;

  /** Get battery Level */
  m_adv_data.battery_level = battery_info.data;

  /** Get Memory Status */
  sh_dl_get_memory_status(&memory_status);
  m_adv_data.used_memory = memory_status.num_pages;

  memset(&init, 0, sizeof(init));

  manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;
  manuf_specific_data.data.p_data = (uint8_t *)&m_adv_data;
  manuf_specific_data.data.size   = sizeof(sh_ble_adv_data_t);   

  init.advdata.p_manuf_specific_data = &manuf_specific_data;
  init.advdata.name_type             = BLE_ADVDATA_FULL_NAME;

  init.config.ble_adv_fast_enabled  = true;
  init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
  init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

  init.evt_handler = on_adv_evt;

  err_code = ble_advertising_init(&m_advertising, &init);
  APP_ERROR_CHECK(err_code);

  ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

void update_conn_interval(void *p_event_data, uint16_t event_size) {

    ble_gap_conn_params_t *p_params = (ble_gap_conn_params_t *)p_event_data;

    uint16_t conn_param_update_cnt = 0;
    while (ble_conn_params_change_conn_params(m_conn_handle, p_params) == NRF_ERROR_BUSY && conn_param_update_cnt++ < 300) {
      nrf_delay_ms(100);
      NRF_LOG_INFO("Waiting for params_change");
      NRF_LOG_FLUSH();
    }
}

/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    app_sched_execute();
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

static void dl_pages_reset(void) {

    // Reset index used for the data logging
    biop_page_index = 0;
    biop_buf_index  = 0;
}

static void enable_modules(void)
{
    nrf_gpio_pin_set(TPS_3V_CTRL_PIN);
    nrf_gpio_pin_set(TPS_ADS1299_1_5V_CTRL_PIN);
    nrf_gpio_pin_set(TPS_ADS1299_2_5V_CTRL_PIN);
    nrf_delay_ms(MODULES_PW_TURN_ON_MS); // Wait for modules to have stable power
}

static void disable_modules(void)
{
    nrf_gpio_pin_clear(TPS_3V_CTRL_PIN);
    nrf_gpio_pin_clear(TPS_ADS1299_1_5V_CTRL_PIN);
    nrf_gpio_pin_clear(TPS_ADS1299_2_5V_CTRL_PIN);
    nrf_gpio_cfg_default(ADS1299_SS_PIN);
    nrf_delay_ms(MODULES_PW_TURN_OFF_MS);
}

/*wrapper function to match the signature of bmm150.write */
lsm6dsl_ret_code_t imu_aux_spi_write(uint8_t id, uint8_t reg_addr, uint8_t *aux_data, uint16_t len) {

  int32_t rslt = LSM6DSL_SUCCESS;
  uint8_t failed_cnt = 0;

  if(!m_modules_initalized) {
    return LSM6DSL_ERROR_COM_FAIL;
  }

  m_tx_buf[0] = reg_addr;
  for (int i = 0; i < len; i++) {
    m_tx_buf[i + 1] = aux_data[i];
  }
  spi_xfer_done = false;

  nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, len + 1, NULL, 0);

  nrf_gpio_pin_clear(IMU_SS_PIN);
  rslt = nrfx_spim_xfer(p_spi, &xfer_desc, 0);
  if(rslt != NRFX_SUCCESS) {
      return LSM6DSL_ERROR_COM_FAIL;
  }
 
  while (spi_xfer_done == false && failed_cnt < SPI_FAILED_CNT_LIMIT) {
    __WFE();
    failed_cnt++;
  }

  nrf_gpio_pin_set(IMU_SS_PIN);
  if(failed_cnt >= SPI_FAILED_CNT_LIMIT) {
      NRF_LOG_INFO("IMU SPI CNT FAILED: %d",failed_cnt);
      return LSM6DSL_ERROR_COM_FAIL;
  }

  return rslt;
}

/*wrapper function to match the signature of imu.read */
lsm6dsl_ret_code_t imu_aux_spi_read(uint8_t id, uint8_t reg_addr, uint8_t *aux_data, uint16_t len) {

  int32_t rslt = LSM6DSL_SUCCESS;
  uint8_t failed_cnt = 0;
  spi_xfer_done = false;

  if(!m_modules_initalized) {
    return LSM6DSL_ERROR_COM_FAIL;
  }

  m_tx_buf[0] = reg_addr;
  nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, 1, m_rx_buf, len +1);

  nrf_gpio_pin_clear(IMU_SS_PIN);

  rslt = nrfx_spim_xfer(p_spi, &xfer_desc, 0);
  if(rslt != NRFX_SUCCESS) {
      return LSM6DSL_ERROR_COM_FAIL;
  }

  while (spi_xfer_done == false && failed_cnt < SPI_FAILED_CNT_LIMIT) {
    __WFE();
    failed_cnt++;
  }

  nrf_gpio_pin_set(IMU_SS_PIN);
  if(failed_cnt >= SPI_FAILED_CNT_LIMIT) {
      NRF_LOG_INFO("IMU SPI CNT FAILED: %d", failed_cnt);
      return LSM6DSL_ERROR_COM_FAIL;
  }

  for (int i = 0; i < len; i++) {
    aux_data[i] = m_rx_buf[i + 1];
  }
  return rslt;
}

/** @brief Perform initial FIFO configuration: disable the FIFO, empty any
 * sample data already in the FIFO, and setup the FIFO interrupt.
 *
*/
static int8_t config_fifo()
{
    lsm6dsl_ret_code_t ret;

    /* set the FIFO to bypass mode and set odr in order to disable FIFO */
    ret = lsm6dsl_fifo_mode_set(LSM6DSL_BYPASS_MODE, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }
    ret = lsm6dsl_fifo_data_rate_set(LSM6DSL_FIFO_DISABLE, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    /* set block data update (BDU) to ensure read full values from FIFO */
    ret = lsm6dsl_block_data_update_set(1, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    /* setup the INT1 for the FIFO threshold */
    lsm6dsl_int1_route_t int1 = {
        .int1_fth = 1,
    };
    ret = lsm6dsl_pin_int1_route_set(int1, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    return ret;
}

static int8_t imu_reset_fifo(void)
{
    lsm6dsl_ret_code_t ret;

    /* reset the FIFO by setting bypass and then FIFO mode */
    ret = lsm6dsl_fifo_mode_set(LSM6DSL_BYPASS_MODE, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }
    ret = lsm6dsl_fifo_mode_set(LSM6DSL_STREAM_TO_FIFO_MODE, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }
}

static int8_t imu_module_init() {
    int8_t ret = SH_SUCCESS;

    /* initialize the IMU device configuration */
    m_imu_dev.read_reg = imu_aux_spi_read;
    m_imu_dev.write_reg = imu_aux_spi_write;

    /* basic device initialization */
    ret = lsm6dsl_init(&m_imu_dev);
    if(ret != LSM6DSL_SUCCESS) {
      NRF_LOG_INFO("LSM6DSL Init Failed");
      return SH_FAILED;
    } else {
      NRF_LOG_INFO("LSM6DSL Init Sucess");
    }

    /* turn off accel */
    ret = lsm6dsl_xl_data_rate_set(LSM6DSL_XL_ODR_OFF, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    /* turn off gyro */
    ret = lsm6dsl_gy_data_rate_set(LSM6DSL_GY_ODR_OFF, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    /* configure the FIFO */
    ret = config_fifo();
    if( ret != SH_SUCCESS) {
      return SH_FAILED;
    }
    /* set the interrupt pin to be active low */
    ret = lsm6dsl_pin_polarity_set(LSM6DSL_ACTIVE_LOW, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    /* set accel full scale */
    ret = lsm6dsl_xl_full_scale_set(LSM6DSL_2g, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    /* set the FIFO threshold */
    ret = lsm6dsl_fifo_watermark_set(ACCEL_FIFO_TH_SAMPLES, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    /* set no decimation for accel data in FIFO */
    ret = lsm6dsl_fifo_xl_batch_set(LSM6DSL_FIFO_XL_NO_DEC, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
      return SH_FAILED;
    }

    return ret;
}

static void start_sensor_modules(void)
{
    ret_code_t err_code;

    // Check if the modules have already started
    if (m_modules_initalized) {
        return;
    }

    NRF_LOG_INFO("Start Modules");
    m_modules_initalized = true;

    // Initalize BIOP Filter
   arm_float_to_q31(firCoeffsf32, firCoeffsq31, ECG_FILT_NUM_TAPS);
    for(int i = 0; i < ADS1299_MAX_NUM_CHNLS; i++) {
        arm_fir_init_q31(&S[i], ECG_FILT_NUM_TAPS,(q31_t *)&(firCoeffsq31[i]), &(firStateQ31[i][0]), ADS1299_BLOCK_SIZE);
    }

    // Init biop chips
    biop_init_devs();

    ads1299_wdt = 0;
    biop_buf_index = 0;
    biop_page_index = 0;

    // IMU Module
//    imu_buf_index = 0;
//    lsm6dsl_wdt = 0;
//
//    err_code = imu_module_init();
//    if(err_code == SH_SUCCESS) {
//      imu_start();
//      sys_status.chest.lsm6dsl = MODULE_SUCCESS;
//    }
//    else {
//      sys_status.chest.lsm6dsl = MODULE_FAIL;
//    }
//    
//    NRF_LOG_FLUSH();




}

static void stop_sensor_modules(void)
{

    // Check if modules have not started
    if (!m_modules_initalized) {
        return;
    }

    NRF_LOG_INFO("Stop Modules");

    m_modules_initalized = false;

    biop_uninit_devs();

    // Digital communication uninit
    nrf_gpio_pin_clear(ADS1299_RESET_PIN);
    nrf_gpio_pin_clear(TPS_ADS1299_1_5V_CTRL_PIN);
    nrf_gpio_pin_clear(TPS_ADS1299_2_5V_CTRL_PIN);
    
}

static int8_t imu_start(void) {
    ret_code_t ret;

    /* turn on the accel at desired rate */
    ret = lsm6dsl_xl_data_rate_set(LSM6DSL_XL_ODR_416Hz, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
        return SH_FAILED;
    }

    /* reset the FIFO by setting bypass and then setting FIFO mode & FIFO ODR */
    ret = lsm6dsl_fifo_mode_set(LSM6DSL_BYPASS_MODE, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
        return SH_FAILED;
    }

    ret = lsm6dsl_fifo_data_rate_set(LSM6DSL_XL_ODR_416Hz, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
        return SH_FAILED;
    }

    ret = lsm6dsl_fifo_mode_set(LSM6DSL_STREAM_TO_FIFO_MODE, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
        return SH_FAILED;
    }
    
    return ret;
}

static int8_t imu_stop(void) {
    ret_code_t ret = SH_SUCCESS;

    /* turn off accel */
    ret = lsm6dsl_xl_data_rate_set(LSM6DSL_XL_ODR_OFF, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
        return SH_FAILED;
    }

    /* turn off gyro */
    ret = lsm6dsl_gy_data_rate_set(LSM6DSL_GY_ODR_OFF, &m_imu_dev);
    if( ret != LSM6DSL_SUCCESS) {
        return SH_FAILED;
    }

    return ret;
}

static void pmic_int_handler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action) {

    NRF_LOG_INFO("PMIC_INT_HANDLER");
    interrupt_flag = true;
}

/**@brief Function for changing the tx power.
 */
static void tx_power_set(void)
{
    ret_code_t err_code = sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, m_advertising.adv_handle, TX_POWER_LEVEL);
    APP_ERROR_CHECK(err_code);
}

/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{
    //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
}

void wdt_init()
{
    ret_code_t err_code;

    //Configure WDT.
    nrfx_wdt_config_t config = NRFX_WDT_DEAFULT_CONFIG;
    config.reload_value = WDT_RELOAD_VAL_MS;
    err_code = nrfx_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrfx_wdt_enable();
}

static void sensor_init(void) {

    ret_code_t err_code;

    if (!m_active_session) {
      enable_modules();
      start_sensor_modules();
      err_code = sh_dl_enable();
      if (err_code != SH_DL_SUCCESS){
        sys_status.fetal.nand_flash = MODULE_FAIL;
      }
      else {
        sys_status.fetal.nand_flash = MODULE_SUCCESS;
      }

      // Module WDT Start
      err_code = app_timer_start(m_module_wdt_timer_id, MODULE_WDT_INTERVAL, NULL);
      if( err_code != NRF_SUCCESS) {
          NRF_LOG_INFO("WDT app timer start error: %d", err_code);
      }


    }

}

static void sensor_uninit(void) {
    ret_code_t err_code;

    if (!m_active_session){

        // App Timer Stop
        app_timer_stop(m_module_wdt_timer_id);
        if( err_code != NRF_SUCCESS) {
          NRF_LOG_INFO("Module WDT app timer stop error: %d", err_code);
        }
        app_timer_stop(m_session_timer_id);
        if( err_code != NRF_SUCCESS) {
          NRF_LOG_INFO("Session app timer stop error: %d", err_code);
        }

        stop_sensor_modules();
        sh_dl_disable();
        disable_modules();

    }
}


static void system_status_init(void)
{
    ret_code_t err_code;

    // Start Sensors
    enable_modules();
    start_sensor_modules();
    err_code = sh_dl_enable();
    if (err_code != SH_DL_SUCCESS){
      sys_status.limb.nand_flash = MODULE_FAIL;
    }
    else {
      sys_status.limb.nand_flash = MODULE_SUCCESS;
    }

    // Stop Sensors
    stop_sensor_modules();
    sh_dl_disable();
    disable_modules();
}

static void timer_init(void)
{
    ret_code_t err_code;

    // Initialize timer module.
    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_pmic_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                pmic_status_timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_module_wdt_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                module_wdt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_session_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                session_end_handler);
    APP_ERROR_CHECK(err_code);
}

static void timestamp_init(void)
{
    uint32_t err_code;

    ts_params.sync_timer = NRF_RTC2;    //Timer is decided on app_config.h
    ts_params.count_timer = NRF_TIMER2;
    ts_params.egu = NRF_EGU3;
    ts_params.egu_irq_type = SWI3_EGU3_IRQn;
    ts_params.ppi_chg = 0;
    ts_params.ppi_chns[0] = 1;
    ts_params.ppi_chns[1] = 2;
    ts_params.ppi_chns[2] = 3;
    ts_params.ppi_chns[3] = 4;
    ts_params.rf_chn = 125; /* For testing purposes */

    err_code = ts_init(&ts_params);
    APP_ERROR_CHECK(err_code);
}

static int8_t data_logger_init(void) {
  ret_code_t err_code;
  sh_dl_params_st dl_params;

  // initalize peripheral
  nrfx_spim_config_t spi_config = NRFX_SPIM_DEFAULT_CONFIG;

  // General Configuration
  spi_config.frequency  = SPI_FREQUENCY_FREQUENCY_M8;
  spi_config.mode       = NRF_SPIM_MODE_3; ///< SCK active low, sample on trailing edge of clock.
  spi_config.bit_order  = NRF_SPIM_BIT_ORDER_MSB_FIRST;
  spi_config.ss_pin     = MT29F_SS_PIN;
  spi_config.miso_pin   = MT29F_MISO_PIN;
  spi_config.mosi_pin   = MT29F_MOSI_PIN;
  spi_config.sck_pin    = MT29F_SCK_PIN;

  dl_params.p_nand_spi_config   = &spi_config;
  dl_params.spi                 = &dl_spi;
  dl_params.dl_cmd_user_handler = on_dl_cmd;

  biop_nand_pages[0].section.header.flag      = SH_INVALID_FLAG;
  biop_nand_pages[1].section.header.flag      = SH_INVALID_FLAG;
  biop_nand_pages[0].section.header.data_type = SH_DATA_TYPE_BIOP_ADS1299_MODE_1;
  biop_nand_pages[1].section.header.data_type = SH_DATA_TYPE_BIOP_ADS1299_MODE_1;

  err_code = sh_dl_init(&dl_params);
  return err_code;
}

// TWI (with transaction manager) initialization.
static void twi_mngr_init(void)
{
    uint32_t err_code;

    nrf_drv_twi_config_t const config = {
       .scl                = BQ_SCL_PIN,
       .sda                = BQ_SDA_PIN,
       .frequency          = NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = NRFX_TWI_DEFAULT_CONFIG_IRQ_PRIORITY, 
       .clear_bus_init     = true,
       .hold_bus_uninit    = NRFX_TWI_DEFAULT_CONFIG_HOLD_BUS_UNINIT
    };

    err_code = nrf_twi_mngr_init(&m_nrf_twi_mngr, &config); 
    APP_ERROR_CHECK(err_code);
}

static int8_t pmic_init()
{
    ret_code_t err_code;

    /* register functions for bq25120 */
    pmic_dev.delay_ms = nrf_delay_ms;
    pmic_dev.read     = pmic_twi_read;
    pmic_dev.write    = pmic_twi_write;
    
    /* Set Device address */
    pmic_dev.m_device_address              = BQ25120_DEVICE_ADDRESS;

    /* Set the default value*/
    pmic_dev.para.batt_voltage             = 4.2f;                           //Set battery voltage 
    pmic_dev.para.in_limit_current         = BQ25120_IN_LIMIT_CURRENT_100ma;  //Set Input Current Limit 
    pmic_dev.para.max_charge_current       = BQ25120_FASTCHARGE_CURRENT_60ma;//Set Charge current
    pmic_dev.para.termination_current      = BQ25120_Termin_CURRENT_2ma;     //Set termination and pre-charge current
    pmic_dev.para.buvlo_voltage            = BQ25120_BAT_UVLO_3_0V;          //Set Battery UVLO voltage
    pmic_dev.para.vindpm_voltage           = BQ25120_VINDPM_DISABLED;        //Set VINDPM voltage
    pmic_dev.para.safe_timer               = BQ25120_Tim_Disable;              //Set Safety Timer Time Limit
    pmic_dev.para.sys_out_vol              = BQ25120_SYS_OUT_1_800V;         //Set SYS OUT voltage
    pmic_dev.para.ld_sw_vol                = BQ25120_LS_LDO_Voltage_DISABLE; //Set Load Switch/LDO voltage

    /* init bq25120a device*/
    nrf_gpio_pin_set(BQ_CD_PIN);
    nrf_delay_ms(BQ_ENABLE_DELAY_MS);
    /* Get rid of bq25120 fail for new battery */
    bq25120_shipmode_fun(Disabled,&pmic_dev);
    err_code = bq25120_init(&pmic_dev);
    nrf_gpio_pin_clear(BQ_CD_PIN);
    if(err_code != BQ25120_OK) {
      NRF_LOG_INFO("BQ25120 Init Failed");
      sys_status.fetal.bq25120 = MODULE_FAIL;
      return SH_FAILED;
    }

    err_code = app_timer_start(m_pmic_timer_id, PMIC_READ_INTERVAL, NULL);
    if( err_code != NRF_SUCCESS) {
        NRF_LOG_INFO("PMIC app timer start error: %d", err_code);
    }
    NRF_LOG_INFO("BQ25120 Init Success");

    return SH_SUCCESS;
}

static void gpio_init() {

    /** ADS1299 Related Pin */
    nrf_gpio_cfg_output(ADS1299_1_PWDN_PIN);
    nrf_gpio_pin_clear(ADS1299_1_PWDN_PIN);

    nrf_gpio_cfg_output(ADS1299_2_PWDN_PIN);
    nrf_gpio_pin_clear(ADS1299_2_PWDN_PIN);

    nrf_gpio_cfg_output(ADS1299_RESET_PIN);
    nrf_gpio_pin_clear(ADS1299_RESET_PIN);

    nrf_gpio_cfg_output(ADS1299_START_PIN);
    nrf_gpio_pin_clear(ADS1299_START_PIN);

    /** Indication LED */
    nrf_gpio_cfg_output(TPS_3V_CTRL_PIN);
    nrf_gpio_pin_clear(TPS_3V_CTRL_PIN);

    nrf_gpio_cfg_output(TPS_ADS1299_1_5V_CTRL_PIN);
    nrf_gpio_pin_clear(TPS_ADS1299_1_5V_CTRL_PIN);

    nrf_gpio_cfg_output(TPS_ADS1299_2_5V_CTRL_PIN);
    nrf_gpio_pin_clear(TPS_ADS1299_2_5V_CTRL_PIN);

    /** Indication LED */
    nrf_gpio_cfg_output(GREEN_INDICATION_LED_PIN);
    nrf_gpio_pin_clear(GREEN_INDICATION_LED_PIN);

    nrf_gpio_cfg_output(RED_INDICATION_LED_PIN);
    nrf_gpio_pin_clear(RED_INDICATION_LED_PIN);


    nrf_gpio_cfg_output(BQ_CD_PIN);
    nrf_gpio_pin_clear(BQ_CD_PIN);
}

/**@brief Function for the Event Scheduler initialization.
 */
static void scheduler_init(void) {
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

/**@brief Function for application main entry.
 */
int main(void)
{

    // UHMS-103 Turning off DCDC 
    NRF_POWER->DCDCEN = 1;

    // Initialize.
    log_init();
    timer_init();
    scheduler_init();
    wdt_init();

    gpio_init();

    // Peripheral Modules System check 
    power_management_init();
    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    conn_params_init();
    peer_manager_init();

    // Init TWI Manager
    twi_mngr_init();

    // Initialize PMIC
    pmic_init();

//    if (!is_bat_critical())
//    {
//        ret_code_t err_code;
//
//        err_code = app_timer_create(&m_crit_bat_timer_id, APP_TIMER_MODE_REPEATED, bat_crit_check_timeout_hndlr);
//        APP_ERROR_CHECK(err_code);
//
//        err_code = app_timer_start(m_crit_bat_timer_id, PMIC_IS_BAT_CRIT_READ_INTERVAL, NULL);
//        APP_ERROR_CHECK(err_code);
//
//        while (is_bat_critical())
//        {
//            idle_state_handle();
//        }
//
//        app_timer_stop(m_crit_bat_timer_id);
//    }

    // Modules Init
    timestamp_init();
    data_logger_init();

    system_status_init();

    // Start execution.
    NRF_LOG_INFO("Fetal Start");
    read_pmic_status();
    ble_bas_battery_level_update(&m_bas, battery_info.data, BLE_CONN_HANDLE_ALL);
    advertising_start(false);

    tx_power_set();

    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}
