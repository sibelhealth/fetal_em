#ifndef PIN_CONFIG_H
#define PIN_CONFIG_H

#define VERSION_MAJOR 1
#define VERSION_MINOR 0

#define HW_VER              "1.0"
#define FW_VER              "0.2.0"

#define SESSION_TIMEOUT     APP_TIMER_TICKS(1000* 60 * 60 * 8)

/**< TX Power Level value. This will be set both in the TX Power service, in the advertising data, and also used to set the radio transmit power. */
#define TX_POWER_LEVEL                      (4)  

/** BQ25120A */
#define BQ_SCL_PIN           NRF_GPIO_PIN_MAP(0,14)
#define BQ_SDA_PIN           NRF_GPIO_PIN_MAP(1,8)
#define BQ_CD_PIN            NRF_GPIO_PIN_MAP(0,16)
#define BQ_INT_PIN           NRF_GPIO_PIN_MAP(0,28)

/** Memory */
#define MT29F_SS_PIN          NRF_GPIO_PIN_MAP(0,3)
#define MT29F_SCK_PIN         NRF_GPIO_PIN_MAP(0,17)
#define MT29F_MISO_PIN        NRF_GPIO_PIN_MAP(0,8)
#define MT29F_MOSI_PIN        NRF_GPIO_PIN_MAP(1,0)

/** SPI */
#define SENSOR_MOSI_PIN     NRF_GPIO_PIN_MAP(0,25)
#define SENSOR_MISO_PIN     NRF_GPIO_PIN_MAP(0,11)
#define SENSOR_SCK_PIN      NRF_GPIO_PIN_MAP(0,27)

/** ADS1299*/
#define ADS1299_RESET_PIN    NRF_GPIO_PIN_MAP(0,30)
#define ADS1299_SS_PIN       NRF_GPIO_PIN_MAP(0,19)
#define ADS1299_START_PIN    NRF_GPIO_PIN_MAP(0,6)
#define ADS1299_MOSI_PIN     SENSOR_MOSI_PIN
#define ADS1299_MISO_PIN     SENSOR_MISO_PIN
#define ADS1299_SCK_PIN      SENSOR_SCK_PIN

#define ADS1299_NUM_DAISY_CHAINS          1        //num independent communication buses to ADS1299
#define ADS1299_NUM_DEVS_PER_DAISY_CHN    2        //num devices on a given daisy chain (must be same on all daisy chains)

/** ADS1299 #1 */
#define ADS1299_1_DRDY_PIN     NRF_GPIO_PIN_MAP(0,5)
#define ADS1299_1_PWDN_PIN     NRF_GPIO_PIN_MAP(0,29)

/** ADS1299 #2 */
#define ADS1299_2_PWDN_PIN     NRF_GPIO_PIN_MAP(0,4)

/** IMU */
#define IMU_SS_PIN             NRF_GPIO_PIN_MAP(0,15)
#define IMU_INT_1              NRF_GPIO_PIN_MAP(0,31)

/** TPS */
#define TPS_3V_CTRL_PIN           NRF_GPIO_PIN_MAP(0,23)
#define TPS_ADS1299_1_5V_CTRL_PIN NRF_GPIO_PIN_MAP(0,26)
#define TPS_ADS1299_2_5V_CTRL_PIN NRF_GPIO_PIN_MAP(0,2)

/* LED */
#define RED_INDICATION_LED_PIN    NRF_GPIO_PIN_MAP(0,22)
#define GREEN_INDICATION_LED_PIN  NRF_GPIO_PIN_MAP(0,24)

/*Choose one of the Radio mode*/
#define TS_RX_MODE 0
#define TS_TX_MODE 1

/*Choose one of timer mode*/
#define TS_TIMER_MODE 0
#define TS_RTC_MODE 1

#define TS_DEBUG_PIN_NUM        2
#define TS_DEBUG_GPIOTE_NUM     3
#define TS_DEBUG_GPIOTE_TASK    NRF_GPIOTE_TASKS_OUT_3
#define TS_DEBUG_PPI_CHAN       NRF_PPI_CHANNEL0
#define TS_DEBUG_RX_PIN_NUM     28

#if (TS_TIMER_MODE)   //TIMER MODE
    #define SYNC_TIMER NRF_TIMER3
#elif (TS_RTC_MODE)
   #define SYNC_TIMER NRF_RTC2
#endif
#endif // PIN_CONFIG_H